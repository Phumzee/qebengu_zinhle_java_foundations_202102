package com.psybergate.grad2021.core.exceptions.hw4;

public class AccountAlreadyExistsException extends Throwable {

    public AccountAlreadyExistsException() {
        System.out.println("Account Already Exists");
    }
}
