package com.psybergate.java.langoverview.oo_basics.hw4a1;

import java.util.ArrayList;

public class Customer {
    private static final int MAX_AGE = 18;
    private String customerType;
    private String customerNum;
    private String name;
    private int age;

    private ArrayList<Customer>company = new ArrayList<>();
    private ArrayList<Customer>people = new ArrayList<>();
    public static int getMaxAge(){
        return MAX_AGE;
    }


    public Customer(String customerNum, String customerType, String name, int age){
        this.customerNum = customerNum;
        this.customerType= customerType;
        this.name = name;
        this.age = age;
        ageValid();
        if (!ageValid()) {
            throw new RuntimeException("Invalid Age!");
        }

    }

    public void addPerson(Customer c) {
        if  (customerType == "Person")
       people.add(c);
    }

    public void addCompany(Customer c){
        if (customerType == "Company")
            company.add(c);
    }

    public ArrayList getPeople(){
        return people;
    }

    public ArrayList getCompanies(){
        return company;
    }


    public boolean ageValid(){
        if (age < MAX_AGE){
            return true;
        }else {
            return false;
        }
    }
}

