package com.psybergate.jeefnds.enterprise.hwent3.service;

import com.psybergate.jeefnds.enterprise.hwent3.entity.Audit;
import com.psybergate.jeefnds.enterprise.hwent3.entity.Customer;

import javax.ejb.Local;

@Local
public interface CustomerService {

  public boolean saveResource(Customer customer, Audit audit);

}
