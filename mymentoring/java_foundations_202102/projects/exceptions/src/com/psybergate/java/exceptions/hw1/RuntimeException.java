package com.psybergate.java.exceptions.hw1;

import com.psybergate.java.exceptions.ce2.MyRuntimeException;

public class RuntimeException {

  public static void main(String[] args) {
    someMethod01();
  }

  public static void someMethod01(){
    someMethod02();
  }

  public static void someMethod02(){
    someMethod03();
  }

  public static void someMethod03(){
    someMethod04();
  }

  public static void someMethod04(){
    someMethod05();
  }

  public static  void someMethod05(){
    throw new MyRuntimeException();
  }

}
