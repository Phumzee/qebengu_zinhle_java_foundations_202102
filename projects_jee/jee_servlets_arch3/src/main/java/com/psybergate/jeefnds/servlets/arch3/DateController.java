package com.psybergate.jeefnds.servlets.arch3;

import javax.ejb.Local;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class DateController implements Controller{

    public void  getCurrentDate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
       resp.setContentType("text/html");
       PrintWriter out = resp.getWriter();
       out.print("<html><body>");
       out.print("Todays date is " + LocalDate.now());
       out.print("</body></html>");
    }


    public void  getTomorrowsDate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Date date = new Date();
      LocalDateTime tomz =   LocalDateTime.from(date.toInstant()).plusDays(1);
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.print("<html><body>");
        out.print("Todays date is " + tomz);
        out.print("</body></html>");
    }


}
