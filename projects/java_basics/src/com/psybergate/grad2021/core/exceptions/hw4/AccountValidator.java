package com.psybergate.grad2021.core.exceptions.hw4;

import com.psybergate.grad2021.core.exceptions.hw4.AccountAlreadyExistsException;
import com.psybergate.grad2021.core.exceptions.hw4.AccountDB;
import com.psybergate.grad2021.core.exceptions.hw4.AccountDoesNotExistException;

public class AccountValidator {

    public void deleteAccount(String accountNum) throws AccountDoesNotExistException {
        boolean found = false;
      for (int i = 0; i <   AccountDB.accounts.size(); ++i){
          if (accountNum == AccountDB.accounts.get(i).getAccountNum()){
              found =true;
          }
      }
       if (found == false ){
           throw new AccountDoesNotExistException();
       }
    }

    public static boolean accountExist(String accountNum) throws AccountAlreadyExistsException {
        if (AccountDB.contains(accountNum)){
            throw new AccountAlreadyExistsException();
           // return  true;
        }
        return false;
    }


}
