package com.psybergate.jeefnds.servlets.arch3;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class DispatcherServie extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><body>");

        String param1 = req.getParameter("greeting");
        String param2 = req.getParameter("date");
        String name = req.getParameter("name");
        GreetingController greetingController = new GreetingController();

        if (param1.equals("hello")) {
            try {
                out.println(runHelloWorld(name));
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }


        }
    }
        public String runHelloWorld(String name) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
            GreetingController greetingController = new GreetingController();
            Method method = greetingController.getClass().getMethod("helloWorld");
            String s1 = method.invoke(greetingController).toString() + name;
            return s1;
        }

        public String runGoodBye(String name) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
            GreetingController greetingController = new GreetingController();
            Method method = greetingController.getClass().getMethod("goodbyeWorld");
            String s1 = method.invoke(greetingController).toString() +" " + name;
            return s1;
        }





    }

