package com.psybergate.core.collections.ce1.v1;

public class Object {
    public String name;
    public  Object next;

    public Object(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Object{" +
                "name='" + name + '\'' +
                ", next=" + next +
                '}';
    }
}
