package com.psybergate.java.langoverview.oobasics_part2.hw5a.hw5a3;

public class Product {
    private String productName;
    private double price;

    public String getProductName() {
        return productName;
    }

    public double getPrice() {
        return price;
    }

    public Product(String name, double price) {
        this.productName = name;
        this.price = price;
    }

    public int getProducts() {
        return 1;
    }
}
