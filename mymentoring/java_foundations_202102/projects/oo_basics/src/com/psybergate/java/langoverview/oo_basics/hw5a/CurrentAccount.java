package com.psybergate.java.langoverview.oo_basics.hw5a;

public class CurrentAccount extends Account {

    private double overdraft;

    public CurrentAccount(String accountNum, double balance, double overdraft){
        super(accountNum,balance);
        this.overdraft = overdraft;
    }

}
