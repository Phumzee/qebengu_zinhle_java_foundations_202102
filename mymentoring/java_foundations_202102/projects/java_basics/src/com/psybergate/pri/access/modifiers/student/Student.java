package com.psybergate.pri.access.modifiers.student;

import com.psybergate.pri.acces.modifiers.person.Person;

public class Student extends Person {

    public Student(String name, String dob, int age) {
        super(name, dob, age);
    }

    public static void main(String[] args) {
        Student s = new Student("jodo", "1222",22);
        System.out.println(s.name);
        System.out.println(s.age);
         s.returnAge();

    }
}
