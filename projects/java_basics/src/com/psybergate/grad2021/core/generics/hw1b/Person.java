package com.psybergate.grad2021.core.generics.hw1b;

public class Person {

    private String personNum;
    private String name;
    private int age;
    private String position;

    public Person(String personNum, String name, int age, String position) {
        this.personNum = personNum;
        this.name = name;
        this.age = age;
        this.position = position;
    }

    public String getPersonNum() {
        return personNum;
    }

    public void setPersonNum(String personNum) {
        this.personNum = personNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPosition() {
        return position;
    }
}
