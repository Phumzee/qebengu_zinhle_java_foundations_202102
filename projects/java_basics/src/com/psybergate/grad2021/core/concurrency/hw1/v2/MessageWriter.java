package com.psybergate.grad2021.core.concurrency.hw1.v2;

import java.util.Random;

public class MessageWriter {

    private  MessageCache messageCache = MessageCache.getINSTANCE();

    public synchronized void write(){
        int i=0;
        do{
        Random random = new Random();
        String s = "Random String: " + random.nextInt(1000);
        System.out.println("s = " + s);
        messageCache.add(s);++i;}
        while (i < 5);

    }

}
