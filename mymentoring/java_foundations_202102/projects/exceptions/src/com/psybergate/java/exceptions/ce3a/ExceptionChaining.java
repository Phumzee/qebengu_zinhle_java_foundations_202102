package com.psybergate.java.exceptions.ce3a;

public class ExceptionChaining {

  public static void main(String[] args) {
    try {
      method2();
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  public static  void nullString(){

    String s1 = null;
    s1.concat("def");
  }

  public  static void method2() throws Exception {
    try{
    nullString();


  }catch(NullPointerException e){
      System.out.println("Caught a Null Pointer Exception.");
      throw new Exception();
    }

}}
