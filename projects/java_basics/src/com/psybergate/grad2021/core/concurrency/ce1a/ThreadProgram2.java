package com.psybergate.grad2021.core.concurrency.ce1a;

public class ThreadProgram2 extends  Thread{

    private Subtracter subtracter;

    public ThreadProgram2(Subtracter subtracter) {
        this.subtracter = subtracter;
    }

    public void run(){
        subtracter.subtract(21);
        System.out.print("Program2 is running");
    }
}
