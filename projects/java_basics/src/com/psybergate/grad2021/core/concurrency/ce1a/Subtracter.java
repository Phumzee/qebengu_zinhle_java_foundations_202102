package com.psybergate.grad2021.core.concurrency.ce1a;

public class Subtracter {

    private int value;

    public Subtracter(int value) {
        this.value = value;
    }

    public void subtract(int num){
        if (num > value){
            throw new IllegalArgumentException("num too large!!");
        }
        if ( value > 50){
        value -= num;
        }
        else {
            value += num;
        }
    }

    public int getValue(){
        return value;
    }
}
