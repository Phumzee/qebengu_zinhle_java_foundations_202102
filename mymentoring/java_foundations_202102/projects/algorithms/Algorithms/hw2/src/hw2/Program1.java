package hw2;

public class Program1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		long start = System.nanoTime();
		for (int i=2 ; i < 100 ; ++i) {
			if (isPrimes(i)) {
				System.out.println(i);
			}
			
		}
		long end = System.nanoTime();
		System.out.println(end - start);
		countFirst();
		//countSecond();
		//countThird();

	}
	
	public static void countFirst() {
		long start = System.nanoTime();
		for (int i=2 ; i < 20000 ; ++i) {
			if (isPrimes(i)) {
				//System.out.println(i);
			}
			
		}
		long end = System.nanoTime();
		System.out.println(end - start);
		
	}
	
	public static void countSecond() {
		long start = System.nanoTime();
		for (int i=2 ; i < 200000 ; ++i) {
			if (isPrimes(i)) {
				//System.out.println(i);
			}
			
		}
		long end = System.nanoTime();
		System.out.println(end - start);
		
	}
	
	public static void countThird() {
		long start = System.nanoTime();
		for (int i=2 ; i < 2000000 ; ++i) {
			if (isPrimes(i)) {
				//System.out.println(i);
			}
			
		}
		long end = System.nanoTime();
		System.out.println(end - start);
		
	}
	
	
	public static boolean isPrimes(int n) {
		int icount = 0;
		for (int i=1 ; i <= n ; ++i) {
			if (n%i == 0) {
				++icount;
			 }
			
		}
		if (icount == 2) {
		return true;
		}else {
			return false;
		}
	}

}
