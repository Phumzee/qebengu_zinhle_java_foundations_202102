package com.psybergate.grad2021.core.concurrency.ce3a.v1;

public class Adder {

    private int startValue;
    private int endValue;


    public Adder(int startValue, int endValue) {
        this.startValue = startValue;
        this.endValue = endValue;
    }

    public int add(int num) {
        for (int i = startValue+1; i <= endValue ; ++i) {
            startValue += i;
        }
        return startValue;
    }


}

