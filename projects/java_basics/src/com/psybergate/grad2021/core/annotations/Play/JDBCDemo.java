package com.psybergate.grad2021.core.annotations.Play;

import java.sql.*;
public class JDBCDemo {
    public static class JdbcDemo {


        private final String conUrl = "jdbc:postgresql://localhost:5432/postgres";
        private final String username = "postgres";
        private final String password = "admin";

        public Connection dbcon() {
            Connection dbcon = null;

            try {
                dbcon = DriverManager.getConnection(conUrl, username, password);
                System.out.println("You are now connected to the server");
            }catch(SQLException e){
                System.err.println(e.getMessage());
            }

            return dbcon;
        }


        public static void main(String[] args) {

            JdbcDemo app = new JdbcDemo();
            Car car = new Car("Hey","Mitsubishi");

            long id = app.insertCar(car);

            System.out.println(
                    String.format("%s, %s car has been inserted with an id of %d",
                            car.getCarname(), car.getBrand(), id));
        }



        public long insertCar(Car car) {
            String SQLinsert = "INSERT INTO tblcars(carname,brand) "
                    + "VALUES(?,?)";

            long id = 0;

            try(Connection dbcon = dbcon();
                PreparedStatement prepareStatement = dbcon.prepareStatement(SQLinsert, Statement.RETURN_GENERATED_KEYS)){
                prepareStatement.setString(1, car.getCarname());
                prepareStatement.setString(2, car.getBrand());

                int rowsAffected = prepareStatement.executeUpdate();

                if(rowsAffected > 0) {

                    try (ResultSet rs = prepareStatement.getGeneratedKeys()){
                        if (rs.next()) {
                            id = rs.getLong(1);
                        }
                    }catch (SQLException ex) {
                        System.out.println(ex.getMessage());
                    }
                }


            }catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return id;
        }
    }
}
