package com.psybergate.java.langoverview.oobasics_part2.demo2a.version2;

public class Student {
    private String studentNumber;
    private String name;
    private int yearOfStudy;

    public Student(String studentNumber, String name, int yearOfStudy) {
        this.studentNumber = studentNumber;
        this.name = name;
        this.yearOfStudy = yearOfStudy;
    }

    public boolean equals(Object obj){
        if (this == obj){
            return true;}
        if (this == null){
            return false; }
        if (!(this instanceof Student)){
            return false;}
        Student s = ((Student) obj);

        return ((s.studentNumber == studentNumber) &&

         (s.name == name) &&

             (s.yearOfStudy == yearOfStudy));

            }
    }


