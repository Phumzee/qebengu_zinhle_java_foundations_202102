package com.psybergate.grad2021.core.generics.hw2;

import java.util.ArrayList;
import java.util.List;

import  com.psybergate.grad2021.core.generics.hw2.classforjarpackage.GreaterThan;

import static com.psybergate.grad2021.core.generics.hw2.GreaterThan.greaterThan1000;

public class Client {

    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(2000);
        numbers.add(500);
        numbers.add(120);
        numbers.add(3500);
        System.out.println("greaterThan1000(numbers) = " + greaterThan1000(numbers));
    }
}
