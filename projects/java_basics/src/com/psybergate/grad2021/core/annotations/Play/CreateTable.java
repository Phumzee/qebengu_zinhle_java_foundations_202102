package com.psybergate.grad2021.core.annotations.Play;

import java.sql.*;

public class CreateTable {
    public static void main(String[] args) {
        Connection c = null;
        Statement stmt = null;
        String CreateSql = null;

        try

        {

            Class.forName("org.postgresql.Driver");

            c = DriverManager.getConnection(

                    "jdbc:postgresql://localhost:5432/postgres",

                    "postgres", "admin");

            System.out.println("Database Connected ..");

            stmt = c.createStatement();

            CreateSql = "Create Table Actor(id int primary key, firstname varchar, lastname varchar, movie varchar) ";

            stmt.executeUpdate(CreateSql);

            stmt.close();
            c.close();

        }

        catch (Exception e)

        {

            System.err.println( e.getClass().getName()+": "+ e.getMessage() );

            System.exit(0);

        }

        System.out.println("Table Created successfully");

    }

}




