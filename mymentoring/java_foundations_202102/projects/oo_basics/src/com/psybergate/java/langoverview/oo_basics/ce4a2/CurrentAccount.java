package com.psybergate.java.langoverview.oo_basics.ce4a2;

import com.psybergate.java.langoverview.oo_basics.ce2a.Account;

public class CurrentAccount extends Account {
    private double interestRate;
    private double overdraft;
    private static final double MAX_OVERDRAFT = 100_000;
    private static String accountType = "Current Account";

    public CurrentAccount(String accountNum, String name, String surname, double balance, double interestRate, double overdraft){
      super(accountNum,name,surname,balance);
      this.interestRate = interestRate;
      this.overdraft = overdraft;

    }

    public boolean isOverdrawn(){
        if (balance < 0){
           return true;
        } else {
            return false;
        }
    }
    public boolean needsToBeReviewed(){
        if (isOverdrawn() && balance> 0.2 * overdraft || balance < -50000  ){
            return true;
        }else {
            return false;
        }
    }




    public  String getAccountType(){
        return accountType;
    }






}
