package e1a;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.*;
import java.nio.file.Files;

public class ToByte {
	
	public static byte[] getBytes(File f) throws FileNotFoundException, IOException {
		byte [] buffer = new byte[1024];
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		FileInputStream fis = new FileInputStream(f);
		int read;
		while ((read = fis.read(buffer)) != -1) {
			os.write(buffer, 0, read);
			
		}
		fis.close();
		os.close();
		
	
	return os.toByteArray();	
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		File f = new File("text01.txt");
		bytes [] bytesFromFile = getBytes(f);
		
		

	}

}
