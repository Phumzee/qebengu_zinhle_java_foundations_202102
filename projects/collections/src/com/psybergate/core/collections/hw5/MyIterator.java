package com.psybergate.core.collections.hw5;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyIterator implements Iterator
{
   private  int position;
   private Object[] elements;

    public MyIterator(SortedStack sortedStack) {
        this.elements = sortedStack.elements;
        this.position = sortedStack.size-1;
    }

    @Override
    public boolean hasNext() {
        return position >= 0;
    }

    @Override
    public Object next() {
        if (!hasNext()) {
            throw new NoSuchElementException("No elements in the stack.");
        }
        return elements[position--];
    }
}
