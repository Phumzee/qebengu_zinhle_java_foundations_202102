package com.psybergate.core.collections.hw4.v3;

//import com.psybergate.core.collections.hw4.v2.Object;

import java.util.*;

public class MyLinkedList implements List {


    private Node head;


    @Override
    public int size() {
       int i;
        Node focusNode = new Node(null);
        focusNode.next = head;
        for ( i=0; focusNode.next != null; ++i) {
            focusNode = focusNode.next;
        }

        return i;
    }


    @Override
    public boolean isEmpty() {
        if (size() == 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean contains(java.lang.Object o) {
        int size = 0;
        if (isEmpty()){
            return false;
        }else {
        Node focusNode = head;
        for (int i = 0; i < size(); ++i) {
            if (focusNode == o) {
                return true;
            }
            focusNode = focusNode.next;
            ++size;
            if (size >= size()){
                return false;
            }
    }
        }
        return false;
}

    @Override
    public Iterator iterator() {
        return new MyIterator();
    }

    @Override
    public java.lang.Object[] toArray() {
        return new java.lang.Object[0];
    }



    @Override
    public boolean add(Object o) {
        if (head == null) {
            Node newNode = new Node(o);
            head = newNode;


        } else if (head.next == null) {
            Node newNode = new Node(o);
            head.next = newNode;
        } else {
            Node  focusNode = head;
            while (focusNode.next != null) {
                focusNode = focusNode.next;
            }
            Node newNode = new Node(o);
            focusNode.next = newNode;
            return true;
        }
        return false;
    }

    @Override
    public boolean remove(java.lang.Object o) {
        Node   focusNode = head;

            for (Iterator iterator = iterator(); iterator().hasNext();){
                if (focusNode == o){
                    Node nextNode = focusNode.next;
                    focusNode.next = nextNode.next;
                    return true;

            }
            focusNode = focusNode.next;

        }
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        return false;
    }

    @Override
    public void clear() {}
     private class MyIterator implements  Iterator{
          Node focusNode = head;
         @Override
         public boolean hasNext() {
             if (head == null ){
                 return false;
             }
             if(head.next == null){
                 return false;
             }
             return focusNode.next!= null;
         }

         @Override
         public Object next() {
             if (!hasNext()) {
                 throw new NoSuchElementException();
             }
             return focusNode.next.element;
         }
     }



    @Override
    public java.lang.Object get(int index) {
        int icount = 0;
        if (isEmpty()){
            throw new IndexOutOfBoundsException();
        }
        if (index == 0) {
            return head.element;
        } else if (index > 0 && index < size()) {
            Node focusNode = head;
            while (icount < index) {
                focusNode = focusNode.next;
                ++icount;
            }
            return focusNode.element;
        } else if (index < 0 || index > size()) {
            throw new IllegalArgumentException();
        }
        else {
            return false;
        }
    }


    @Override
    public java.lang.Object set(int index, java.lang.Object element) {
        int icount = 0;
        if (isEmpty()){
            throw new IndexOutOfBoundsException();
        }
        if (index == 0) {
            Node removeNode = head;
            Node  newNode = new Node(element);
            newNode.next = head;
            head = newNode;
            return removeNode.element;

        } else if (index > 0 && index < size()) {
           Node prevNode = new Node(null);
            prevNode.next = head;
            Node  focusNode = head;
            //++icount;
            while (icount < index) {
                focusNode = focusNode.next;
                prevNode = prevNode.next;
                ++icount;
            }
            Node   removeNode = focusNode;
            Node newNode = new Node(element);
            newNode.next = focusNode.next;
           prevNode.next = newNode;
            return removeNode.element;
    }
        else if (index >= size()){
            throw new IndexOutOfBoundsException() ;
        }

        else{
            return null;
        }
    }

    @Override
    public void add(int index, java.lang.Object element) {
        int icount = 0;
        if (index == 0) {
            Node  newNode = new Node(element);
            newNode.next = head;
            head = newNode;

        } else {
            Node  focusNode = head;
            ++icount;
            while (icount < index) {
                focusNode = focusNode.next;
                ++icount;
            }
            Node newNode = new Node(element);
            newNode.next = focusNode.next;
            focusNode.next = newNode;

        }

    }

    @Override
    public java.lang.Object remove(int index) {

        Node focusNode = head;
        for (int i=0 ; i < index -1; i++){
           focusNode = focusNode.next;
        }

        Object nextNode = focusNode.next.element;
        focusNode.next = focusNode.next.next;
        return nextNode;
    }

    @Override
    public int indexOf(java.lang.Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(java.lang.Object o) {
        return 0;
    }

    @Override
    public ListIterator listIterator() {
        return null;
    }

    @Override
    public ListIterator listIterator(int index) {
        return null;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public java.lang.Object[] toArray(java.lang.Object[] a) {
        return new java.lang.Object[0];
    }


}