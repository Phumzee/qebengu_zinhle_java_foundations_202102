package com.psybergate.java.langoverview.oobasics_part2.hw3a;

public class CustomerUtils {
    public static void main(String[] args) {
        Customer c1 = new Customer("123", "Person", "She", 12);
        Customer c2 = new Customer("123", "Person", "Her", 14);
        Customer c3 = new Customer("124", "Person", "Hey", 13);

        System.out.println(c1 == c2);
        System.out.println(c1 == c3);
        System.out.println(c1.equals(c2));
        System.out.println(c1.equals(c3));

    }
}
