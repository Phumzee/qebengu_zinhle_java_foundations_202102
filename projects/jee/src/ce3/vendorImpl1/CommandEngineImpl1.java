package ce3.vendorImpl1;

import ce3.standards.CommandEngine;
import ce3.standards.CommandRequest;
import ce3.standards.CommandResponse;

public class CommandEngineImpl1 implements CommandEngine  {

    CommandRequest  commandRequest;

    public CommandEngineImpl1(CommandRequest commandRequest) {
        this.commandRequest = commandRequest;
    }

    public void start(){
        commandRequest.execute();
    }
}
