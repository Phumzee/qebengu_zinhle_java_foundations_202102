package ce1a.vendor1;

import ce1a.standard.Date;
import ce1a.standard.InvalidDateException;

import java.time.LocalDate;

import static ce1a.standard.InvalidDateException.DAY_INVALID;
import static ce1a.standard.InvalidDateException.YEAR_INVALID;

public class MyDateImpl1 implements Date {

    int year;
    int month;
    int day;


    public MyDateImpl1(int day, int month, int year) throws InvalidDateException {
        if (day <= 0 || day> 31) {
            throw new InvalidDateException(DAY_INVALID, "Day invalid");

        }
        if (month <=0 || month > 12 ){
            throw new InvalidDateException(InvalidDateException.MONTH_INVALID, " Month invalid");

        }
        if (year < 1900 || year > 2500){
            throw new InvalidDateException(YEAR_INVALID, "Year invalid");}
        if (isLeapYear() && month ==2 & day> 29){
            throw new InvalidDateException(DAY_INVALID, "Day Invalid");
        }
        if (!isLeapYear() && month ==2 & day >28){
            throw new InvalidDateException(DAY_INVALID, "Day Invalid.");
        }
        if (month == 4 || month == 6 || month == 9 || month == 11){
            if (day > 30){
                throw new InvalidDateException(DAY_INVALID, "Day Invalid");
            }
        }
        this.year = year;
       this.month = month;
       this.day = day;}




    @Override
    public int getYear() {
        return year;
    }

    @Override
    public int getMonth() {
        return month;
    }

    @Override
    public int getDay() {
        return day;
    }

    @Override
    public boolean isLeapYear() {
        if (year % 100 != 0 && year % 4 == 0 ){
            return true;
        }
        if (year % 100 == 0 && year % 400 == 0){
            return true;
        }
        return false;
    }

    @Override
    public Date addDays(int numDays) throws InvalidDateException {
      LocalDate d4  =  LocalDate.of(year, month,day).plusDays(numDays);
      String s1 = d4.toString();
      String [] strings = s1.split("-");
      year = Integer.parseInt(strings[0]);
      month = Integer.parseInt(strings[1]);
      day = Integer.parseInt(strings[2]);
      System.out.println(day);
      Date d = new MyDateImpl1(day,month,year);
        return d;
    }

    @Override
    public String toString() {
        return year +
                "-" + month +
                "-" + day
                ;
    }

}
