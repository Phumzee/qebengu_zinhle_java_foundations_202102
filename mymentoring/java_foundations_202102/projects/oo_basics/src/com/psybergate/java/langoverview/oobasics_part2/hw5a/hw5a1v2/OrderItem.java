package com.psybergate.java.langoverview.oobasics_part2.hw5a.hw5a1v2;

//import com.psybergate.java.langoverview.oobasics_part2.hw5a.String;

import java.util.ArrayList;
import com.psybergate.java.langoverview.oobasics_part2.hw5a.hw5a1v2.Product;

public class OrderItem {

    private int orderId;
    private int producId;
    private String productName;
    private int quantity;
    private Product product;

    public ArrayList<Product> getProducts() {
        return products;
    }

    private ArrayList<OrderItem> orderItems = new ArrayList<>();
    private  ArrayList<Product> products = new ArrayList<Product>() {
        {
            add(new Product(1,"iPhone0", 250000.00));
            add(new Product(2,"iPhone1", 12000.00));
            add(new Product(3,"iPhone2", 14500.00));
            add(new Product(4,"iPhone3", 15000.00));
            add(new Product(5,"iPhone4", 16000.00));
            add(new Product(6,"iPhone5", 18000.00));
        }};



    public OrderItem(int orderId, int quantity, int productId) {
        this.orderId = orderId;
        this.quantity = quantity;
        this.product = product;

    }

    public int getProducId() {
        return producId;
    }

    public String getProductName( int productId){
        String productName = " ";
        for (int i=0 ; i< products.size(); ++i ){
            if (productId == products.get(i).getProductId()){
                productName = products.get(i).getProductName();
            }
        }
        return productName;
    }


    public int getQuantity() {
        return quantity;
    }

    public double orderItemTotal(){
       double productPrice  = 0;

//      for (int i=0 ; i< products.size(); ++i ){
//            if (productName == products.get(i).getProductName()){
//                productPrice = products.get(i).getProductPrice();
//            }
//    }
        return product.productPrice * quantity;
    }

//    public void addOrderItems(){
//
//    }







}
