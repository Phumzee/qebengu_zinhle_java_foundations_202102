package com.psybergate.jeefnds.servlets.arch2_v2;

import javax.servlet.http.HttpServletRequest;

public interface Controller {

    public String execute(HttpServletRequest req);
}
