package com.psybergate.core.collections.hw1;

import java.util.Comparator;

public class DecreasingOrder implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        Customer e1 = (Customer) o1;
        Customer e2 = (Customer) o2;
        return e2.getCustomerNum() - e1.getCustomerNum();
    }
}
