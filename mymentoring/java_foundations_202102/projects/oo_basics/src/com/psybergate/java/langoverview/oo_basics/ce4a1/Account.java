package com.psybergate.java.langoverview.oo_basics.ce4a1;

public class Account {
    private String accountNum;
    protected double balance;

    public Account(String accountNum, double balance){
        this.accountNum = accountNum;
        this.balance = balance;
    }

    public void print(){
        System.out.println("This is an account");
    }


}
