package com.psybergate.java.langoverview.oobasics_part2.hw5a.hw5a3;

public class OrderItem {

    private int quantity;
    private Product product;

    public OrderItem(int quantity, Product product) {
        this.quantity = quantity;
        this.product = product;
    }

    public OrderItem(int quantity, com.psybergate.java.langoverview.oobasics_part2.hw5a.hw5a1v2.Product iPhone0) {
    }

    public OrderItem(int orderId, int quantity, int productId) {
    }

    public int getQuantity() {
        return quantity;
    }

    public double OrderItemTotal(){
        return product.getPrice() * quantity;
    }



}
