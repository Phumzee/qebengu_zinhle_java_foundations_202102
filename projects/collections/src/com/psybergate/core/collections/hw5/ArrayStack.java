package com.psybergate.core.collections.hw5;

import com.psybergate.core.collections.ce1.v2.AbstractStack;

import java.util.Iterator;

public class ArrayStack extends AbstractStack implements Iterable {

    Object[] elements = new Object[1000];
    int size = 0;

    @Override
    public void push(Object o) {
        elements[this.size] = o;
        ++size;

    }

    @Override
    public Object pop() {
        if (size == 0) {
            return null;
        }
        Object popped = elements[this.size - 1];
        elements[size - 1] = null;
        --size;
        return popped;

    }

    @Override
    public Object get(int position) {

        int icount = 0;
        while (icount != position) {
            ++icount;
        }

        return elements[icount];
    }

    public int size() {
        return this.size;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public boolean contains(Object o) {
        int size = 0;
        for (Iterator iterator = iterator(); iterator().hasNext(); ) {
            Object object = iterator.next();
            if (object.equals(o)) {
                return true;
            }
            ++size;
            if (size >= size()) {
                return false;
            }
        }

        return false;
    }

    public boolean add(Object o) {
        if (o != null) {
            return true;
        }
        return false;
    }



//    private class MyIterator implements Iterator {
//        int position = 0;
//
//        @Override
//        public boolean hasNext() {
//            return position <= size;
//        }
//
//        @Override
//        public java.lang.Object next() {
//            return elements[position++];
//        }
//    }


}
