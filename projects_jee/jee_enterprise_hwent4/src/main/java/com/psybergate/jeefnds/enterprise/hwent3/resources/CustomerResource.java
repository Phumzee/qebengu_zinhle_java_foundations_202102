package com.psybergate.jeefnds.enterprise.hwent3.resources;

import com.psybergate.jeefnds.enterprise.hwent3.entities.Customer;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@ApplicationScoped
public class CustomerResource {

  @PersistenceContext
  private EntityManager entityManager;

  public void save(Customer customer) {
    entityManager.persist(customer);
  }
}
