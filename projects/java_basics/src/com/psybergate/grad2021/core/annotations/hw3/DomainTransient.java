package com.psybergate.grad2021.core.annotations.hw3;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
// if this is not here, will not be available at Runtime (cannot be accessed via reflection)
@Target( {ElementType.FIELD})
 @interface DomainTransient {
    boolean isNull() default true;

    boolean PrimaryKey() default false;
}
