package com.psybergate.java.langoverview.oobasics_part2.demo2a.version2;

import com.psybergate.java.langoverview.oobasics_part2.demo2a.Student;

public class Main {
    public static void main(String[] args) {
        Student s1 = new Student("123", "Zinhle", 3);
        Student s2 = new Student("123", "Zinhle", 3);
        Student s3 = new Student("124", "P", 2);
        Student s4 = s3;

        System.out.println(s1.equals(s2));
        System.out.println(s1.equals(s3));
        System.out.println(s3.equals(s4));
        System.out.println(s1.equals(s1));
    }
}
