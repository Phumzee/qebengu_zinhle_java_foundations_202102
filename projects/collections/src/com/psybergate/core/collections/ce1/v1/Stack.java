package com.psybergate.core.collections.ce1.v1;

import com.psybergate.core.collections.ce1.v1.Object;

import java.util.Collection;

public interface Stack extends Collection {
    public void push(Object o);
    public Object pop();
    public Object get(int position);




}
