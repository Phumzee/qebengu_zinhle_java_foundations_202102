package com.psybergate.grad2021.core.innerclasses.hw2.AnnonWithoutClass;

abstract class Person {
    abstract void eat();



static class TestAnonymousInner$ extends  Person{

    @Override
    void eat() {
        System.out.println("Nice fruits.");
    }
}}
