package com.psybergate.core.enums.hw1;

public class StudentWithEnum {

  private int studentNumber;
  private String name;
  private String course;
  private YearOfStudy1 yearOfStudy;


  public StudentWithEnum(int studentNumber, String name, String course, YearOfStudy1 yearOfStudy) {
    this.studentNumber = studentNumber;
    this.name = name;
    this.course = course;
    this.yearOfStudy = yearOfStudy;
  }

  public boolean isFirstYear(){
    return yearOfStudy == YearOfStudy1.FIRST_YEAR;
  }

  public boolean isSecondYear(){
    return yearOfStudy == YearOfStudy1.SECOND_YEAR;
  }

  public boolean isThirdYear(){
    return yearOfStudy == YearOfStudy1.THIRD_YEAR;
  }
}
