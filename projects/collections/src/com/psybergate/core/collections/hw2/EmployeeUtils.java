package com.psybergate.core.collections.hw2;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;



public class EmployeeUtils {
  public static void main(String[] args) {

    Map m1 = new TreeMap();
    Employee1 e1 = new Employee1(2, "Zee", "manager", 12000.00);
    Employee1 e2 = new Employee1(3, "Noxy", "manager", 10000.00);
    Employee1 e3 = new Employee1(1, "Pearl" ,  "manager", 5000.00);
    Employee1 e4 = new Employee1(3, "Jodo" ,  "manager", 6000.00);
    m1.put(e1, e1.getSalary()*12);
    m1.put(e2, e2.getSalary()*12);
    m1.put(e3, e3.getSalary()*12);
    m1.put(e4, e4.getSalary()*12);



    printMap(m1);
    System.out.println("geAnnualSalary(m1, e1) = " + geAnnualSalary(m1, e1));
    System.out.println("m1.get(e1) = " + m1.get(e1));


  }
  public static  void printMap(Map m1){
    System.out.println("map= " + m1);
    Set keys = m1.keySet();
    int i = 0;
    for (final Object key : keys){
      System.out.println("map" + i++ + "(" + key + ") = " + m1.get(key));
    }}

    public static Double geAnnualSalary(Map m1, Employee1 e1){
     double annualSalary = e1.getSalary() * 12;
     return annualSalary;

    }


  }

