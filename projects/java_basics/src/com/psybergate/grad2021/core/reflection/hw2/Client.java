package com.psybergate.grad2021.core.reflection.hw2;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;


public class Client {

    public static void main(String[] args) throws IOException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {

        DynamicLoader.loadStream();

        Class clazz = DynamicLoader.getClass("ArrayList");

        List<String> months = DynamicLoader.populateList(clazz);

       PrintUtils.printList(months);


    }}
