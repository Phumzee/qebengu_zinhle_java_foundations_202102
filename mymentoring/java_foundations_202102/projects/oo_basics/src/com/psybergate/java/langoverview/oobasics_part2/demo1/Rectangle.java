package com.psybergate.java.langoverview.oobasics_part2.demo1;

public class Rectangle extends Shape {


    //Could not override constructor. No use of super.
    public Rectangle(String dimensions, double area) {
        super(dimensions, area);
        System.out.println("I am a Rectangle");

    }

    public static boolean isPolygon(){
        System.out.println("Static method in Rectangle.");
        return numOfSides > 1;
    }


}
