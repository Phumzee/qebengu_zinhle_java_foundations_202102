package com.psybergate.grad2021.core.annotations.hw4;


import com.psybergate.grad2021.core.annotations.hw3.Customer;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseManager {
    public static void main(String[] args) throws SQLException {
     connectDB();
        System.out.println("createSql() = " + createSql());

    }


    public static Connection connectDB() throws SQLException {
        Connection conn = null;
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
                            "postgres", "admin");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Opened database successfully");
//       Statement statement = conn.createStatement();


  return conn;
    }

    public static String createSql(){
      String createStmt = "CREATE TABLE CUSTOMER("  ;
      Class clazz = Customer.class;
      Annotation[] annotations2 = clazz.getAnnotations();
        System.out.println("annotations2.length = " + annotations2.length);

      Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            System.out.println("field = " + field);
            Annotation[] annotations = field.getDeclaredAnnotations();

            for (Annotation annotation : annotations) {
                //System.out.println("annotation = " + annotation);
                if (annotation instanceof DomainProperty){
                    String constraints = "";
                    if (((DomainProperty) annotation).Primarykey() == true){
                        constraints = constraints + "PRIMARY KEY ";
                    }
                    if (((DomainProperty) annotation).isNull() == false && ((DomainProperty) annotation).Primarykey()==false ){
                        constraints = constraints + "NOT NULL";
                    }

                    createStmt = createStmt +  field.getName() + " " + getType(field) +" " + constraints;
                }
            }
          if (field.getName() != "dateOfBirth"){
              createStmt = createStmt + ",";
          }

        }
         createStmt = createStmt + ")";
      return  createStmt;
    }


    public static String getType(Field field){
        String type = "";
      if (field.getType().equals(String.class)){
          type = "TEXT";
      }
      if (field.getType().equals(int.class)){
          type = "INT";
      }
      if (field.getType().equals(Integer.class)){
          type = "INT";
      }


       return type;
    }
}
