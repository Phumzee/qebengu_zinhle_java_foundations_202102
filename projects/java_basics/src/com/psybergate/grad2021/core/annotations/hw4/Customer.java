package com.psybergate.grad2021.core.annotations.hw4;




public class Customer {

    @DomainProperty(isNull = false, Primarykey = true)
    private String customerNum;

    @DomainProperty
    private String name;

    @DomainProperty
    private String surname;

    @DomainProperty
    //private Integer dateOfBirth;
    private String dateOfBirth;

    @DomainTransient
    private int age;

//    public Customer(String customerNum, String name, String surname, Integer dateOfBirth, int age) {
//        this.customerNum = customerNum;
//        this.name = name;
//        this.surname = surname;
//        this.dateOfBirth = dateOfBirth;
//        this.age = age;
//    }

    public Customer(String customerNum, String name, String surname, String dateOfBirth) {
        this.customerNum = customerNum;
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
    }

    public String getCustomerNum() {
        return customerNum;
    }

    public String getName() {
        return name;
    }


    public String getSurname() {
        return surname;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public int getAge() {
        return age;
    }


    @Override
    public String toString() {
        return "Customer{" +
                "customerNum='" + customerNum + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", age=" + age +
                '}';
    }
}
