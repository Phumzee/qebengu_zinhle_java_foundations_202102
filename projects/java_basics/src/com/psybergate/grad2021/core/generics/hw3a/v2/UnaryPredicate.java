package com.psybergate.grad2021.core.generics.hw3a.v2;

public interface UnaryPredicate<T> {
    public boolean test(T obj);
}
