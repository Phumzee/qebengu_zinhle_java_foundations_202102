package com.psybergate.java.exceptions.ce2;

import static com.psybergate.java.exceptions.ce2.ClassA.*;

public class ClassB {

  public static void main(String[] args) throws MyException {
    someBMethod01();
    someBMethod02();
    someBMethod03();
    someBMethod04();
  }

  public  static void someBMethod01() throws MyException {
    someMethod01();
  }

  public  static void someBMethod02(){
    someMethod02();

  }

  public static void someBMethod03(){
    someMethod03();

  }

  public static void someBMethod04(){
    someMethod04();


  }
}
