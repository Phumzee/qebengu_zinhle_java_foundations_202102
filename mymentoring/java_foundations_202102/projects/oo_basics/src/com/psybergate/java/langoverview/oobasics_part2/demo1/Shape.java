package com.psybergate.java.langoverview.oobasics_part2.demo1;

public class Shape {
    private String dimensions;
    private double area;
    protected static int numOfSides; //this would not be static in the real world.

    public Shape(String dimensions, double area) {
        this.dimensions = dimensions;
        this.area = area;

        System.out.println("I am a shape.");
    }

    public static boolean isPolygon(){
        System.out.println("Static method in class");
        return numOfSides > 1;


    }

}
