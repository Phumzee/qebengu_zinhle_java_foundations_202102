package com.psybergate.java.langoverview.oo_basics.demo2a;

public class Circle extends Shape{


    private int radius;

    public Circle(int radius) {
        super();
        this.radius = radius;
    }

    public double getArea(){
        return 0;
    }



    public String getDimensions(){
        String s1 = "radius: " + radius;
        return s1;
    }
}
