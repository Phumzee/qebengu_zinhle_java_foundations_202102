package com.psybergate.grad2021.core.exceptions.hw4;

public class AccountDoesNotExistException extends Exception{

    public AccountDoesNotExistException() {
        System.out.println("Account does not exist!");
    }
}
