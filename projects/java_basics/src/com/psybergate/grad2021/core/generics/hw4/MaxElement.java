package com.psybergate.grad2021.core.generics.hw4;

import java.util.*;
import java.util.List;

public class MaxElement {
    public static <T extends  Object & Comparable<? super T>> T max(List<? extends T> list, int begin , int end){
        T maxElem = list.get(begin);
        for (++begin; begin < end ; ++begin)
        if (maxElem.compareTo(list.get(begin)) < 0)
              maxElem = list.get(begin);

     return maxElem;
}}
