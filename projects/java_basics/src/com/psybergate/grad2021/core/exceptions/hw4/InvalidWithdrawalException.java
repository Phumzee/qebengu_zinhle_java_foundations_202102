package com.psybergate.grad2021.core.exceptions.hw4;

public class InvalidWithdrawalException extends Exception {

    public InvalidWithdrawalException() {
        System.out.println("Invalid Withdrawal!");
    }

}
