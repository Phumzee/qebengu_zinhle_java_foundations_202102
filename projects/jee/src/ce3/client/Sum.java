package ce3.client;

import ce3.standards.Command;
import ce3.standards.CommandRequest;

public class Sum implements Command {

    private int value1;
    private int value2;

    public Sum(int value1, int value2) {
        this.value1 = value1;
        this.value2 = value2;
    }

    public void execute() {
        int result = value1 + value2;
        System.out.println("result = " + result);
    }
}
