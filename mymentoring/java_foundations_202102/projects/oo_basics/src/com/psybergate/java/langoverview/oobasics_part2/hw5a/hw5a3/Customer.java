package com.psybergate.java.langoverview.oobasics_part2.hw5a.hw5a3;

//import com.psybergate.java.langoverview.oobasics_part2.hw5a.OrderItem;

import java.util.ArrayList;

public class Customer {

    private String name;
    private String customerType;
    private int customerDuration;
    private String discountType;;
    private double discount;


    private ArrayList<Order> orders = new ArrayList<>();

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public Customer(String name, String customerType, int numOfYears,String discountType) {
        this.name = name;
        this.customerType = customerType;
        this.customerDuration = numOfYears;
        this.discountType = discountType;

        // this.orders = orders;


    }
    public String getCustomerType() {
        return customerType;
    }


    public Order makeOrder(ArrayList<OrderItem> orderItems){
        Order order;
      if (customerType == "local") {
           order = new LocalOrder();
          for (int i = 0; i < orderItems.size(); ++i) {
              order.addOrderItems(orderItems.get(i));
              orders.add(order);
          }
      }

      else {
           order = new InternationalOrder();
          for (int i = 0; i < orderItems.size(); ++i) {
              order.addOrderItems(orderItems.get(i));
              orders.add(order);
      }
      }
      return order;
    }


    public int getCustomerDuration() {
        return customerDuration;
    }



    public double getTotal(){
        double total = 0;
        for (int i=0 ; i < orders.size(); ++i){
        total =   orders.get(i).getOrderTotal();
          }
        return total;
    }

    public String getDiscountType() {
        return discountType;
    }}



    

