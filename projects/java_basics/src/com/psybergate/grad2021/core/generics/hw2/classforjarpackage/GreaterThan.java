package com.psybergate.grad2021.core.generics.hw2.classforjarpackage;

import java.util.ArrayList;
import java.util.List;

public class GreaterThan {

    public static  List<Integer>  greaterThan1000(List<Integer> numbers){
        List<Integer> nums = new ArrayList<>();
        for (Integer n: numbers) {
            if (n > 1000){
               nums.add(n);

            }

        }
      return nums;
    }
}
