package com.psybergate.grad2021.core.reflection.hw2;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Properties;

public class DynamicLoader {

    private static final Properties PROPERTIES = new Properties();


    public static void loadStream() throws IOException {
        InputStream stream =
                Client.class.getClassLoader().getResourceAsStream("com/psybergate/grad2021/" +
                                "core/reflection/hw2/properties.txt");

        PROPERTIES.load(stream);
    }


    public static Class getClass(String listName) throws ClassNotFoundException {
        return Class.forName(getProperty(listName));
    }


    static String getProperty(String key) {
        return (String) PROPERTIES.get(key);
    }


    public static List<String> populateList(Class clazz) throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
        List<String> list = (List<String>) clazz.newInstance();

        Method add = clazz.getMethod("add", Object.class);
        add.invoke(list, "January");
        add.invoke(list, "February");
        add.invoke(list, "March");
        add.invoke(list, "April");
        add.invoke(list, "May");
        add.invoke(list, "June");
        add.invoke(list, "July");
        add.invoke(list, "August");
        add.invoke(list, "September");
        add.invoke(list, "October");
        add.invoke(list, "November");
        add.invoke(list, "December");

        return list;
    }

}


