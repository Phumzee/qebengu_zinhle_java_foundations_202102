package com.psybergate.java.langoverview.oobasics_part2.hw5a.hw5a1v2;

import java.text.ParseException;
import java.util.ArrayList;

public class OrderUtils {
  public static void main(String[] args) {
//     ArrayList<Product> products = new ArrayList<Product>() {
//      {
//        add(new Product(1,"iPhone0", 250000.00));
//        add(new Product(2,"iPhone1", 12000.00));
//        add(new Product(3,"iPhone2", 14500.00));
//        add(new Product(4,"iPhone3", 15000.00));
//        add(new Product(5,"iPhone4", 16000.00));
//        add(new Product(6,"iPhone5", 18000.00));
//      }};
    ArrayList<Customer> customers = new ArrayList<>();
    ArrayList<Order> orders = new ArrayList<>();

    Customer c1 = new Customer(customers.size() + 1, "Zee", "international", "2012-02-11");
    customers.add(c1);
    Customer c2 = new Customer(customers.size() + 1, "Noxy", "international", "2012-02-11");
    customers.add(c2);

//        Customer c3 = new Customer( "Pearl", "local","2012-02-11" );
//        Customer c4 = new Customer( "Jodo", "international","2012-02-11" );

    customers.add(c1);
    customers.add(c2);

    Order o1 = new Order(orders.size() + 1, c1, "2020-13-14");
    // o1.makeOrder();
    orders.add(o1);
    Order o2 = new Order(orders.size() + 1, c1, "2020-13-14");
    orders.add(o2);

    Order o3 = new Order(orders.size() + 1, c2, "2020-13-14");
    orders.add(o3);

    Order o4 = new Order(orders.size() + 1, c2, "2020-13-14");
    orders.add(o4);

    Order o5 = new Order(orders.size() + 1, c2, "2020-13-14");
    orders.add(o5);

    Order o6 = new Order(orders.size() + 1, c2, "2020-13-14");

    Order o7 = new Order(orders.size() + 1, c1, "2020-13-14");
    Order o8 = new Order(orders.size() + 1, c1, "2020-13-14");

    o1.addOrderItems(3, 1);
    o2.addOrderItems(3, 2);
    o3.addOrderItems(2, 3);

    o4.addOrderItems(2, 4);
    o5.addOrderItems(2, 1);
    o6.addOrderItems(3, 2);


    System.out.println("o1.getOrderTotal() = " + o1.getOrderTotal());
    System.out.println("o2.getOrderTotal() = " + o2.getOrderTotal());
    System.out.println("o4.getOrderTotal() = " + o4.getOrderTotal());
    System.out.println("c1.getCustomerId() = " + c1.getCustomerId());
    System.out.println("c2.getCustomerId() = " + c2.getCustomerId());
//        o1.addOrderItems(2, 3);
//        o1.addOrderItems(2, 4);


    //  System.out.println(c.getTotal());
    //  System.out.println();


  }

//    public static void print(ArrayList<Customer> customers){
//        for (int i=0 ; i < customers.size(); ++i){
//            System.out.println(customers.get(i).getTotal());
//        }

}


