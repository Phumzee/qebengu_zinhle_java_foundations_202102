package com.psybergate.grad2021.core.concurrency.ce4a;

public class SingletonRunnable implements  Runnable{

    private Singleton singleton = Singleton.getInstance();

    public SingletonRunnable(Singleton singleton) {
        this.singleton = singleton;
    }

    @Override
    public void run() {
      singleton.add(5);
        System.out.println("singleton.getSum() = " + singleton.getSum());

    }
}
