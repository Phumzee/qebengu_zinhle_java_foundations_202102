package com.psybergate.java.exceptions.hw1;

public class MyCheckedException extends Exception{
  public MyCheckedException() {
  }

  public MyCheckedException(String message) {

    super(message);
  }
}
