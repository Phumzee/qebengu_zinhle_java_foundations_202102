package com.psybergate.java.langoverview.oobasics_part2.hw4a;

public class Utils {
    public static void main(String[] args) {

        Child.print(); //To show that static methods are inherited , I commented out the print()
                       // in the Class method and it ran the one in the Parent class
        Parent p = new Child();
        p.print();  //To show that static methods cannot be executed polymorphically, it will run the print()
                     //from the Parent class


    }





}
