package com.psybergate.grad2021.core.concurrency.ce3a.v2;

public class AdderRunnable implements Runnable{

    private Adder adder;
    int value;

    public AdderRunnable(Adder adder) {
        this.adder = adder;
    }

    @Override
    public void run() {
        value = adder.add();
    }

    public int getValue(){
        return value;
    }
}
