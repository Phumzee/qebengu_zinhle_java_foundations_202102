package com.psybergate.java.langoverview.oobasics_part2.hw5a.hw5a1v2;

public class InternationalOrder extends Order {

    //private double importDuties;
    private static final double MORE_THAN_HALFMILLI = 0.05;
     private static final double MORE_THAN_MILLI = 0.1;
     //private double internationalDisocunt;
     private static final double IMPORT_DUTIES = 200;

    public InternationalOrder(int orderId, Customer customer, String orderDate) {
        super(orderId, customer, orderDate);
    }


    public double getDiscount(){
        double internationalDisocunt = 0;

         if (super.getOrderTotal()> 1000000){
            internationalDisocunt = MORE_THAN_HALFMILLI;
        }
        else if (super.getOrderTotal() > 500000){
            internationalDisocunt = MORE_THAN_HALFMILLI;}
        return internationalDisocunt;
    }


    public double getOrderTotal() {
        //getDiscount();
        return (super.getOrderTotal()  * (1 - getDiscount()))+ IMPORT_DUTIES;

    }
}


