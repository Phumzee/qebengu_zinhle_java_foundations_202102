package com.psybergate.grad2021.core.concurrency.hw1.v2;

import java.util.ArrayList;
import java.util.List;

public class MessageCache {

    public static final MessageCache INSTANCE = new MessageCache();

    private List<String> messages = new ArrayList<>();

    public void add(String message) {
        messages.add(message);
    }

    public String read() {
        return messages.get(0);
    }

    public List<String> getMessages() {
        return messages;
    }

    public static MessageCache getINSTANCE() {
        return INSTANCE;
    }

    public boolean isEmpty() {
        return messages.size() == 0;
    }

}
