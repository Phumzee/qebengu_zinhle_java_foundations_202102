package com.psybergate.core.collections.ce1.v2;

import java.util.Collection;

public interface Stack extends Collection {

    public void push(Object o);
    public Object pop();
    public Object get(int position);

}
