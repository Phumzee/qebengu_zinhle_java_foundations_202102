package com.psybergate.grad2021.core.concurrency.ce3a.v1;

public class MyThread extends Thread {
    private int sumValue = 100;
    private int value = 1;
    private Adder adder;
    private static int numThreads = 1;
    private int num = sumValue / numThreads;

    public MyThread(Adder adder) {
        this.adder = adder;
        ++numThreads;
        num = sumValue / numThreads;
    }

    @Override
    public void run() {
        System.out.println("Thread: " + Thread.currentThread().getName() + "is starting.");
        for (int i = 1; i < numThreads; ++i) {
            adder.add(num);
          //  this.value += adder.getValue();
           // System.out.println("value: " + adder.getValue());
        }

    }


}
