package com.psybergate.jeefnds.servlets.hwweb3;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@WebServlet("/dispatch/*")
public class DispatcherServlet extends HttpServlet {

    private static final Map<String, Controller> CONTROLLERS = new HashMap<>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("WEB-INF/savecust.jsp");
        dispatcher.forward(req,resp);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String reqpathInfo = req.getPathInfo();
        String[] strings = reqpathInfo.split("/");
        String controller = strings[1];
        Controller controller1 = getController(controller);
        String method = strings[2];
        try {
            controller1.execute(req, resp,method);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }


    public static void loadControllers() {
        try{
            InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("controller.properties");
            Properties props = new Properties();
            props.load(is);
            for (Map.Entry entry : props.entrySet()){
                Controller controller = (Controller) Class.forName(entry.getValue().toString()).newInstance();
                CONTROLLERS.put(entry.getKey().toString(),controller);

            }
        } catch (Exception ex) {
            throw new RuntimeException("Error - Exception not handled", ex);
        }

    }

    private Controller getController(String reqpathInfo){
        return CONTROLLERS.get(reqpathInfo);
    }

}
