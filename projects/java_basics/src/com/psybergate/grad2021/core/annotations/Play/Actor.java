package com.psybergate.grad2021.core.annotations.Play;

public class Actor {


       private int iD;
        private String firstName;
        private String lastName;
        private String movie;

        private static int counter = 0;


        public Actor(String firstName, String lastName, String movie ) {
            this.iD = ++counter;
            this.firstName = firstName;
            this.lastName = lastName;
            this.movie = movie;

        }

        public Actor() {
        }

        /**
         * @return the firstName
         */
        public String getFirstName() {
            return firstName;
        }

        /**
         * @param firstName the firstName to set
         */
        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        /**
         * @return the lastName
         */
        public String getLastName() {
            return lastName;
        }

        /**
         * @param lastName the lastName to set
         */
        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

    public int getiD() {
        return iD;
    }

    public void setiD(int iD) {
        this.iD = iD;
    }

    public String getMovie() {
        return movie;
    }

    public void setMovie(String movie) {
        this.movie = movie;
    }
}

