package com.psybergate.jeefnds.servlets.hwweb5_v2;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Filter;
import java.util.logging.LogRecord;

@WebFilter("/helloworld/*")
public class MyFilter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {


    }



    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        System.out.println("in filter...");

        String names = servletRequest.getParameter("name");


        if (names.contains("john")) {

            PrintWriter out = servletResponse.getWriter();

            out.println(

                    "<p>" + "john has been passed as a parameter" + "</p>"

            );

        } else {


            filterChain.doFilter(servletRequest, servletResponse);

        }

    }


    public void destroy() {


    }

    @Override
    public boolean isLoggable(LogRecord record) {
        return false;
    }
}
