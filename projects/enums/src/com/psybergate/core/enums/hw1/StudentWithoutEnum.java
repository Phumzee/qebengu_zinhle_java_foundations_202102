package com.psybergate.core.enums.hw1;

import java.time.Year;

public class StudentWithoutEnum {
  private int studentNumber;
  private String name;
  private String course;
  private YearOfStudy yearOfStudy;


  public StudentWithoutEnum(int studentNumber, String name, String course, YearOfStudy yearOfStudy) {
    this.studentNumber = studentNumber;
    this.name = name;
    this.course = course;
    this.yearOfStudy = yearOfStudy;
  }

  public boolean isFirstYear(){
    return yearOfStudy == YearOfStudy.FIRST_YEAR;
  }

  public boolean isSecondYear(){
    return yearOfStudy == YearOfStudy.SECOND_YEAR;
  }

  public boolean isThirdYear(){
    return yearOfStudy == YearOfStudy.THIRD_YEAR;
  }





}
