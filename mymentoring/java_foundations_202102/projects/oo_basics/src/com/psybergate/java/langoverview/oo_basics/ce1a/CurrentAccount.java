package com.psybergate.java.langoverview.oo_basics.ce1a;

public class CurrentAccount {
    private String accountNum;
    private  double balance;

    public double getBalance(){
        return balance;
    }

    public CurrentAccount(String accountNum, double balance){
        this.accountNum = accountNum;
        this.balance = balance;

    }

    public boolean isOverdrawn(){
        if (balance < 0){
            return true;
        }else {
            return false;
        }
    }

}
