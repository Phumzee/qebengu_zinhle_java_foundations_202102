package com.psybergate.java.langoverview.oobasics_part2.hw5a.hw5a1;

import java.util.ArrayList;

public class Customer {
    private String name;
    private String customerType;
    private int numOfYears;


    private ArrayList<Order>orders = new ArrayList<>();// Better to model



    public Customer(String name, String customerType, int numOfYears) {
        this.name = name;
        this.customerType = customerType;
        this.numOfYears = numOfYears;
        // this.orders = orders;


    }
    public String getCustomerType() {
        return customerType;
    }


    public void makeOrder(Order order){
        orders.add(order);
    }


    public int getNumOfYears() {
        return numOfYears;
    }

    public double getTotal(){
        double total = 0;
        for (int i=0 ; i < orders.size(); ++i){

        total =    orders.get(i).getOrderTotal();}

        return total;
    }}



    

