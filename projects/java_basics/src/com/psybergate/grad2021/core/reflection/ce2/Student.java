package com.psybergate.grad2021.core.reflection.ce2;

//import com.psybergate.grad2021.core.reflection.DomainClass;

import java.io.Serializable;

@DomainClass
public class Student implements Serializable {
    public String studentNum;
    private String name;
    private String surname;
    private int age;
    private int yearOfStudy;

    public Student(String studentNum, String name, String surname) {
        this.studentNum = studentNum;
        this.name = name;
        this.surname = surname;
    }

    public Student(String studentNum, String name, String surname, int age, int yearOfStudy) {
        this.studentNum = studentNum;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.yearOfStudy = yearOfStudy;
    }

    public Student() {
        this.studentNum = studentNum;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.yearOfStudy = yearOfStudy;
    }

    public String getStudentNum() {
        return studentNum;
    }

    public void setStudentNum(String studentNum) {
        this.studentNum = studentNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public boolean isStudent(){
        return true;
    }


}
