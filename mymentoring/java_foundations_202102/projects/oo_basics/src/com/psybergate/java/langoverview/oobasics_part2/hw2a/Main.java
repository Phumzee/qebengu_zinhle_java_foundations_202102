package com.psybergate.java.langoverview.oobasics_part2.hw2a;
import java.util.*;
public class Main {
    public static void main(String[] args) {


        Rectangle r1 = new Rectangle(12, 6);
        Rectangle r2 = new Rectangle(12, 6);
        Rectangle r3 = new Rectangle(10, 5);

        System.out.println(r1 == r2);
        System.out.println(r1 == r3);
        System.out.println(r1.equals(r2));
        System.out.println(r1.equals(r3));

    }

}
