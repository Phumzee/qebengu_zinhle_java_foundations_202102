package com.psybergate.grad2021.core.reflection.ce1;


import com.psybergate.grad2021.core.reflection.ce2.Student;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;


public class StudentClient {
    public static void main(String[] args) {
        Class clazz = Student.class;
        System.out.println("clazz = " + clazz);
        System.out.println("clazz.getPackage() = " + clazz.getPackage());
        Annotation[] annotations = clazz.getAnnotations();
        for (Annotation annotation : annotations) {
            System.out.println("annotation = " + annotation.toString());
        }

        System.out.println("clazz.getInterfaces() = " + clazz.getInterfaces());
        Field [] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            System.out.println("field = " + field);
        }
        Constructor[] constructors = clazz.getDeclaredConstructors();
        for (Constructor constructor : constructors) {
            Parameter[] parameters = constructor.getParameters();
            for (Parameter parameter : parameters) {
                System.out.println("parameter = " + parameter);
            }
        }

        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println("method = " + method);
        }

    }



}
