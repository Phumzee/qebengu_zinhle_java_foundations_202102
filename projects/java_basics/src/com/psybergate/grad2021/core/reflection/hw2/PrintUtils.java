package com.psybergate.grad2021.core.reflection.hw2;

import java.util.List;

public class PrintUtils {

    public static void printList(List<String> list) {
        int i = 1;
        for (String month : list) {
            System.out.println(i + ".) " + month);
            i++;
        }
    }

}
