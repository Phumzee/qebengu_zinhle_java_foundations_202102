package com.psybergate.grad2021.core.reflection.hw2;

import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class DynamicLoading {

    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, IOException {
        Class clazz = ArrayList.class;
        System.out.println("clazz.getName() = " + clazz.getName());
        System.out.println("clazz.getInterfaces() = " + clazz.getInterfaces());
        Constructor constructor = clazz.getConstructor();
        List list1 = (List) constructor.newInstance();
        list1.add("January");
        list1.add("February");
        list1.add("March");
        list1.add("April");
        list1.add("May");
        list1.add("June");
        list1.add("July");
        list1.add("August");
        list1.add("September");
        list1.add("October");
        list1.add("November");
        list1.add("December");

        for (Object o : list1) {
            System.out.println("o.toString() = " + o.toString());
        }

        FileWriter myWriter = new FileWriter("properties.txt");
        myWriter.write("java.util.ArrayList");
        myWriter.close();

    }





}
