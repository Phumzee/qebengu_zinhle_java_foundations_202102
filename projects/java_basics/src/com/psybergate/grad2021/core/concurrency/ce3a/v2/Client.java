package com.psybergate.grad2021.core.concurrency.ce3a.v2;

public class Client {
    public static void main(String[] args) throws InterruptedException {
        long startTime = System.nanoTime();
        ConcurrentAdder concurrentAdder = new ConcurrentAdder(10000, 5);
        concurrentAdder.getSumPerInterval();
        concurrentAdder.endOfThread();
        long endTime = System.nanoTime();
        long duration = duration(startTime,endTime);
        System.out.println("concurrentAdder.getTotalSum() = " + concurrentAdder.getTotalSum());
    }

         public  static long duration(long startTime, long endTime){
          return  endTime - startTime;
         }}