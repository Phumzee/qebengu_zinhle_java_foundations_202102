package com.psybergate.java.langoverview.oobasics_part2.ce3a.version2;

public class MoneyUtils {

    public static void main(String[] args) {
        Money m1 = new Money("South Africa", "ZAR");
        Money m2 = new Money("USA", "USSD");
        System.out.println(m1.toString());
        System.out.println(m2.toString());
        m1.changeCode();
        System.out.println(m1.toString());
    }
}
