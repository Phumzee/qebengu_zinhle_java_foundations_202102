package com.psybergate.core.collections.hw4.v2;

public class Object extends java.lang.Object {

    public java.lang.Object element;
    public Object next = null;

    public Object(Object element) {
        this.element = element;
    }

    @Override
    public String toString() {
        return "Node{" +
                "s1='" + element + '\'' +
                ", next=" + next +
                '}';
    }
}
