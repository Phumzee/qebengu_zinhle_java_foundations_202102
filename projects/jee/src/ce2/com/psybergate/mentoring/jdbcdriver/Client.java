package ce2.com.psybergate.mentoring.jdbcdriver;

import java.sql.*;

public class Client {



    public static void main(String[] args) throws SQLException, ClassNotFoundException {

         String url = "jdbc:mydatabase://localhost:1234/customers";
         String user = "postgres";
         String password = "admin";

       Class.forName("com.psybergate.mentoring.jdbcdriver.MyDriver");
       Connection connection = (MyConnection) DriverManager.getConnection(url,user,password);
       Statement statement = (MyStatement) connection.createStatement();
        ResultSet rs = (MyResultSet) statement.executeQuery("select * from mytable");
}}
