package com.psybergate.grad2021.core.concurrency.ce4a;

public class MyThread extends Thread{
    private Singleton singleton;

    public MyThread() {
        this.singleton = Singleton.getInstance();
    }

    public synchronized Singleton getSingleton(){
        return  singleton;
    }
}
