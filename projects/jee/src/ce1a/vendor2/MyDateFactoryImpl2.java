package ce1a.vendor2;

import ce1a.standard.Date;
import ce1a.standard.DateFactory;
import ce1a.standard.InvalidDateException;
import ce1a.vendor1.MyDateImpl1;

public class MyDateFactoryImpl2 implements DateFactory {
    @Override
    public Date createDate(int day, int month, int year) throws InvalidDateException {
            return new MyDateImpl1(day,month, year);
        }
    }

