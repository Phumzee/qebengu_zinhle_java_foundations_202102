package com.psybergate.core.collections.hw5;

//import com.psybergate.core.collections.hw1.DecreasingOrder;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

public class TreeStack extends SortedStack {


    public void push(Object o) {
        add(o);
    }

    public boolean add(Object o) {

        if (size == 0) {
            elements[0] = o;
            ++size;
            return true;
        }
        for (int i = 0; i < size; i++) {
            Comparable s1 = (Comparable) elements[i];
            Comparable s2 = (Comparable) o;
            if (s1.compareTo(s2) > 0) {
                for (int j = size - 1; j >= i; j--) {
                    elements[j + 1] = elements[j];
                }
                elements[i] = o;
                ++size;
                return true;
            }

        }
        elements[size++] = o;
        return true;
    }


    public Iterator iterator() {
        return new MyIterator(this);
    }
}

//    public Object pop() {
//
//        Object popped  = s1.last();
//        s1.remove(popped);
//        return popped;
//
//    }







