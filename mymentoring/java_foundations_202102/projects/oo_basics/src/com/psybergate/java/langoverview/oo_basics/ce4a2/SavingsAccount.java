package com.psybergate.java.langoverview.oo_basics.ce4a2;

public class SavingsAccount extends Account {
    private static double minBalance = 5000;
    private static String accountType = "Savings Account";

    public SavingsAccount(String accountNum, String name, String surname, double balance){
        super(accountNum,name,surname,balance);


    }
    public boolean isOverdrawn(){
        if (balance < 5000){
            return true;
        }else {
            return false;
        }
    }


    @Override
    public boolean needsToBeReviewed() {
        if (balance < 2000 ){
            return true;
        }else {
            return false;
        }

    }

    @Override
    public String getAccountType() {
        return accountType;
    }

}
