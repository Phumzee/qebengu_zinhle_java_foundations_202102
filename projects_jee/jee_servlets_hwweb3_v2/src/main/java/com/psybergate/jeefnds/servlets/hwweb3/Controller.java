package com.psybergate.jeefnds.servlets.hwweb3;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public interface Controller {


    public void execute(HttpServletRequest request, HttpServletResponse response,String method) throws SQLException, IOException, ClassNotFoundException;
}
