package com.psybergate.java.langoverview.oo_basics.ce4a2;

public abstract class Account {
    private String accountNum;
    private String name;
    private String surname;
    protected double balance;

    public Account(String accountNum, String name, String surname, double balance){
        this.accountNum = accountNum;
        this.name = name;
        this.surname = surname;
        this.balance = balance;

    }


    public  abstract boolean needsToBeReviewed();



    public abstract String getAccountType();




}
