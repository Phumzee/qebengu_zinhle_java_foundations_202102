package ce3.client;

import ce3.standards.Command;
import ce3.standards.CommandRequest;

public class Factorial implements Command{

    private  int value;

    public Factorial(int value) {
        this.value = value;
    }

    public void execute(){

        int result = 1;
        for (int i=value ; value > 0 ; --i){
            result = result * value;
        }
        System.out.println("result = " + result);
        //return result;
    }


}
