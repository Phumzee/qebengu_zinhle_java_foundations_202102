package ce1a.vendor1;

import ce1a.standard.Date;
import ce1a.standard.DateFactory;
import ce1a.standard.InvalidDateException;

public class MyDateFactoryImpl1 implements DateFactory {
    @Override
    public  Date createDate(int day, int month, int year) throws InvalidDateException {
            return new MyDateImpl1(day,month, year);
        }
    }

