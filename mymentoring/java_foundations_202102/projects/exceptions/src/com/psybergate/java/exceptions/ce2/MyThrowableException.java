package com.psybergate.java.exceptions.ce2;

public class MyThrowableException extends Throwable {
  public MyThrowableException() {
  }

  public MyThrowableException(String message) {
    super(message);
  }
}
