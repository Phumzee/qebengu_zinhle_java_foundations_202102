package com.psybergate.grad2021.core.concurrency.ce4a;

public class Singleton {


    private static Singleton single_instance = new Singleton();


    private int sum;


    public static Singleton getInstance() {
        if (single_instance == null)
            single_instance = new Singleton();

        return single_instance;
    }

    public int getSum() {
        return sum;
    }

    public synchronized void add(int num){
       sum += num;
    }

}
