package com.psybergate.java.langoverview.oobasics_part2.hw5a.hw5a1v2;

import java.util.ArrayList;

public class Product {
    private int productId;
    private String productName;
    public double productPrice;
    int icount =0;

//    ArrayList<Product> products = new ArrayList<Product>() {
//        {
//            add(new Product(1 ,"iPhone0", 10000.00));
//            add(new Product(2 ,"iPhone1", 12000.00));
//            add(new Product(3 ,"iPhone2", 14500.00));
//            add(new Product(4 ,"iPhone3", 15000.00));
//            add(new Product(5 ,"iPhone4", 16000.00));
//            add(new Product(6 ,"iPhone5", 18000.00));
//        }};

    // int icounter  = products.size();

         public Product(int productId, String name, double price){
         this.productId = productId;
         this.productName = name;
        this.productPrice = price;


     }

//    public Product( int productId, String name, double price) {
//       this(name, price );
//        this.productId = ++icount;
//
//    }
//    public ArrayList<Product> getProducts() {
//        return products;
//    }

    public String getProductName() {
        return productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public int getProductId() {
        return productId;
    }
}
