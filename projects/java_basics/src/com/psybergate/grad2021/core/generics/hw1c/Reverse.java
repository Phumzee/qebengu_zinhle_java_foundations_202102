package com.psybergate.grad2021.core.generics.hw1c;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Reverse {
    public static void main(String[] args) {
        List<String> list1 = new ArrayList<>();
        list1.add("abc");
        list1.add("def");
        list1.add("ghi");
        System.out.println("reverseList(list1) = " + reverseList1(list1));
    }



public static List<String> reverseList(List<String> words){
    List<String> reverse = new ArrayList<>();
    for (String w: words) {
        String reversew = reverseWord(w);
        reverse.add(reversew);
    }

    return reverse;
}


    public static List  reverseList1(List words){
        List<String> reverse = new ArrayList<>();
        for (Object w: words) {
            String reversew = reverseWord1((String) w);
            reverse.add(reversew);
        }

        return reverse;
    }


public static String reverseWord(String word){
    String reverse = " ";
    for (int i= word.length()-1; i>=0; --i){
        reverse = reverse + word.charAt(i);
    }
   return reverse;
}

    public static String reverseWord1(String word){
        String reverse = " ";
        for (int i= word.length()-1; i>=0; --i){
            reverse = reverse + word.charAt(i);
        }
        return reverse;
    }



}