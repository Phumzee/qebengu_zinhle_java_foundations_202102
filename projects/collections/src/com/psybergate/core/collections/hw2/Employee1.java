package com.psybergate.core.collections.hw2;

import java.util.*;

public class Employee1 implements Comparable{
  private int employeeNum;
  private String name;
  private String position;
  private double salary;




  public Employee1(int employeeNum, String name, String position, double salary) {
    this.employeeNum = employeeNum;
    this.name = name;
    this.position = position;
    this.salary = salary;

  }

   public Double getSalary() {
    return salary;
  }

  public int getEmployeeNum() {
    return employeeNum;
  }

  @Override
  public String toString() {
    return "Employee{" +
            "employeeNum='" + employeeNum + '\'' +
            ", name='" + name + '\'' +
            ", position='" + position + '\'' +
            '}';

  }

  @Override
  public int compareTo(Object o) {
    Employee1 e1 = (Employee1) o;
    return (employeeNum - e1.employeeNum);
  }





//  @Override
//  public int hashCode() {
//    return 31 + this.employeeNum;
//  }
//
//
//
//  public boolean equals(Object o){
//    if (this == o){
//      return true;
//    }
//     if (o == null){
//      return false;
//    }
//     if (o.getClass() != this.getClass()){
//       return false;
//     }
//    Employee e = (Employee)o;
//     return this.employeeNum == e.employeeNum;
//  }
//  public static  void printMap(){
//    System.out.println("map= " + m1);
//    Set keys = m1.keySet();
//    int i = 0;
//    for (final Object key : keys){
//      System.out.println("map" + i++ + "(" + key + ") = " + m1.get(key));
//    }

//    public static void printMap(){
//    for (Map.Entry<Object, Double> me : m1.entrySet()) {
//      System.out.println("hey");
//      System.out.print(me.getKey() + ":");
//      System.out.println(me.getValue());
//    }}

  }
