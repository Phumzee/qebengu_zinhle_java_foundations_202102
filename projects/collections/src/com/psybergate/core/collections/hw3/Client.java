package com.psybergate.core.collections.hw3;

import com.psybergate.core.collections.hw1.DecreasingOrder;

import java.util.SortedSet;
import java.util.TreeSet;

//import static com.psybergate.core.collections.hw3.MyString.print;

public class Client {
    public static void main(String[] args) {

        SortedSet s1 = new TreeSet(new DecreasingOrder1());
        s1.add(new String("Zee"));
        s1.add(new String("Noxy"));
        s1.add(new String("Pearl"));
        s1.add(new String("Prudence"));
        print(s1);
    }
    public static void print(SortedSet sortedSet){
        for (Object e : sortedSet){
            System.out.println(e);
        }


}}


