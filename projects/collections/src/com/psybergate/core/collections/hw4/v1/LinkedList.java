package com.psybergate.core.collections.hw4.v1;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class LinkedList implements List {

    public static void main(String[] args) {
        LinkedList li = new LinkedList();
        li.addNode(new Node("abc1"));
        li.addNode(new Node("abc2"));
        li.addNode(new Node("abc3"));
        li.addNode(new Node("abc4"));

        li.findNode(0);
        li.removeNode(1);
        li.findNode(0);
        System.out.println("li.findNode(1) = " + li.findNode(1));
        li.addNode(new Node("abc5"), 1);
        li.findNode(0);
    }

    private Node head;


    public void addNode(Node o) {
        if (head == null) {
            head = o;
        } else if (head.next == null) {
            head.next = o;
        } else {
            Node focusNode = head;
            while (focusNode.next != null) {
                focusNode = (Node) focusNode.next;
            }
            focusNode.next = o;
        }
    }

    public void removeNode(int index) {
        int icount = 0;
        if (index == 0) {
            head = (Node) head.next;
        } else {
            Node focusNode = head;
            ++icount;
            while (icount < index) {
                focusNode = (Node) focusNode.next;
                ++icount;
            }
            Node nextNode = (Node) focusNode.next;
            focusNode.next = nextNode.next;
        }
    }

    public Node findNode(int index) {
        System.out.println(head);
        int icount = 0;
        if (index == 0) {
            return head;
        } else {
            Node focusNode = head;
            while (icount < index) {
                focusNode = (Node) focusNode.next;
                ++icount;
            }
            return focusNode;
        }
    }

    public void addNode(Node o, int index) {
        int icount = 0;
        if (index == 0) {
            Node focusNode = head;
            head = o;
            o.next = focusNode;
        } else {
            Node focusNode = head;
            ++icount;
            while (icount < index) {
                focusNode = (Node) focusNode.next;
                ++icount;
            }
            Node nextNode = (Node) focusNode.next;
            focusNode.next = o;
            o.next = nextNode;

        }


    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public boolean add(Object o) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean equals(Object o) {
        return false;
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public Object get(int index) {
        return null;
    }

    @Override
    public Object set(int index, Object element) {
        return null;
    }

    @Override
    public void add(int index, Object element) {

    }

    @Override
    public Object remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator listIterator() {
        return null;
    }

    @Override
    public ListIterator listIterator(int index) {
        return null;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }
}
