package com.psybergate.grad2021.core.annotations.hw2;

 @interface DomainTransient {
    boolean isNull() default true;

    boolean PrimaryKey() default false;
}
