package com.psybergate.jeefnds.enterprise.hwent3.service;

import com.psybergate.jeefnds.enterprise.hwent3.entity.Audit;
import com.psybergate.jeefnds.enterprise.hwent3.entity.Customer;
import com.psybergate.jeefnds.enterprise.hwent3.resource.AuditResource;
import com.psybergate.jeefnds.enterprise.hwent3.resource.CustomerResource;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class CustomerServiceImpl implements  CustomerService{


    @Inject
    CustomerResource customerResource;

    @Inject
    AuditResource auditResource;


    @Override
    public boolean saveResource(Customer customer, Audit audit) {


        customerResource.save(customer);
        auditResource.save(audit);
        return true;
    }


}
