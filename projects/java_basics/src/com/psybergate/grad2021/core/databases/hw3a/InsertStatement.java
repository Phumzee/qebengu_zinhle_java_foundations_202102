package com.psybergate.grad2021.core.databases.hw3a;

import javax.xml.stream.events.Namespace;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Random;


public class InsertStatement {

    public static  int accountNum = 1;
    public static int transactionNum = 1;
    public static int customerNum = 1;

    public static void insert() throws IOException {
        String sql = "";
        sql = sql + insertIntoTransaction();
        FileWriter myWriter = new FileWriter("transaction.sql");
        myWriter.write(sql);
        myWriter.close();


    }
    public static void main(String[] args) throws IOException {
//        System.out.println("InsertIntoCustomer() = " + InsertIntoCustomer());
//        System.out.println("insertIntoAccountType() = " + insertIntoAccountType());
        //System.out.println("insertIntoAccount() = " + insertIntoAccount());
        //System.out.println("insertIntoTransaction() = " + insertIntoTransaction());
         insert();
//        System.out.println("insertIntoAccountType() = " + insertIntoAccountType());
//        System.out.println("insertIntoTransactionType() = " + insertIntoTransactionType());
//        System.out.println("insertIntoAccount(\"2\") = " + insertIntoAccount("2"));
//        System.out.println("insertIntoTransaction(\"2\") = " + insertIntoTransaction("2"));

    }

//    public static String insertIntoAccountPro(String string){
//     String sql = sql + string;
//
//
//     return sql;
//    }





    public  static String insertIntoAccount(){
        String sql = "INSERT INTO ACCOUNT VALUES";
        int icount = 0;

        for (int i=1 ; i < 21 ; ++i){
            Random random = new Random();
            int int_random = random.nextInt(8);
        for (int j=1 ; j < int_random; ++j){
          sql = sql + "(" + "'" + accountNum + "'" + "," + generateBalance() + "," + "'" + i + "'" + "," ;
          sql = sql + "'" + ((random.nextInt(2)+1)) + "'";
          if ( i< 20 || j <= int_random -2){
              sql = sql + "),";
          }
          ++accountNum;
        }

        }
        sql = sql + ");";
        System.out.println("sql = " + sql);
        return sql;
    }

    public static String InsertIntoCustomer(){
         String[] Names = {"Prudence","Pearl", "Palesa","John","Josh", "Joseph", "James", "Kamo", "Thapelo", "Thabo"};
        String[] Surnames = {"Qebengu", "Tsotesi", "Saley", "Choonara", "Polo", "Jodo", "Knowles", "Chirwa","Mbongo","Dingane"};
        String[] Cities = {"Sebokeng", "Vereeniging", "Jozi", "Maboneng", "London","Durbz","CPT", "Lenasia", "Rustenburg", "Randburg"};
        String sql = "INSERT INTO CUSTOMER VALUES";
        for (int i=1 ; i < 21 ;++i ){
            Random random = new Random();
            int int_random = random.nextInt(10);
            sql = sql + "(" +  "'" + i + "'" + "," + "'" + Names[int_random]+"'" + "," + "'" +  Surnames[int_random] +"'" +",'" + generateDate()+ "',";
            sql = sql +  "'" + Cities[int_random] + "'";
            if (i < 20){
                sql = sql + "),";
            }
        }
        sql = sql + ");";
        return sql;
        
    }

    public static String insertIntoTransactionType(){
      String   transactionTypes [] = {"withdraw", "deposit"};
        String sql = "INSERT INTO TRANSACTIONTYPE VALUES";
        for (int i= 1 ; i < 3 ; ++i){
            sql = sql + "(" + "'" + i + "'" + "," +"'" + transactionTypes[i-1] + "'";
            if (i < 2){
                sql = sql + "),";
            }
        }
        sql = sql + ");";
        return sql;
    }

    public static String insertIntoAccountType(){
        String   accountTypes [] = {"Current", "Savings", "Credit"};
        String sql = "INSERT INTO ACCOUNTTYPE VALUES";
        for (int i= 1 ; i < 3 ; ++i){
            sql = sql +"(" +  "'" + i + "'" + "," +"'" + accountTypes[i-1] + "'";
            if (i < 2){
                sql = sql + "),";
            }
        }
        sql = sql + ");";
        return sql;
    }

    public static String insertIntoTransaction(){
        String sql = "INSERT INTO TRANSACTION VALUES";

        for (int i=1 ; i < 9; ++i ){
            Random random = new Random();
        int int_random = random.nextInt(101);
        for (int j=0 ; j < int_random; ++j){
            sql = sql + "(" + "'" + transactionNum + "'" + "," + generateAmount() + "," + "'" + i + "'" + "," ;
            sql = sql + "'" + (random.nextInt(2)+1) + "'";
            if ( i < 8 ||j <= int_random -2){
                sql = sql + "),";
            }
            ++transactionNum;
        }}

        sql = sql + ");";
        return sql;
    }


    public static double generateBalance(){
        double balance = 0;
        Random random = new Random();
        int min = 10000; //minimum number
        int max = 100000;
        balance = random.nextInt((max - min)+ 1) + min;


      return balance;
    }

    public static double generateAmount(){
        double amount = 0 ;
        Random random = new Random();
        int min = 2000; //minimum number
        int max = 20000;
        amount = random.nextInt((max - min)+ 1) + min;



        return amount;
    }




    public static  LocalDate generateDate(){
        Random random = new Random();
        int minDay = (int) LocalDate.of(1900, 1, 1).toEpochDay();
        int maxDay = (int) LocalDate.of(2015, 1, 1).toEpochDay();
        long randomDay = minDay + random.nextInt(maxDay - minDay);

        LocalDate randomBirthDate = LocalDate.ofEpochDay(randomDay);
        return randomBirthDate;

    }
}
