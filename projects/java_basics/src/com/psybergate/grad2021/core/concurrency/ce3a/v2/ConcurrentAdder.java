package com.psybergate.grad2021.core.concurrency.ce3a.v2;


import java.util.HashMap;
import java.util.Map;

public class ConcurrentAdder {

    private int NumofIntervals;
    private int endNum ;
    private int startRange = 1;
    private int endRange ;
    private int diffRange ;
    private int totalSum = 0;

    public ConcurrentAdder(int endNum, int numofIntervals) {
        this.endNum = endNum;
        this.NumofIntervals = numofIntervals;
        endRange = endNum/NumofIntervals;
        diffRange = endRange;
    }


    Map<Thread, AdderRunnable> maps = new HashMap<>();

      public void getSumPerInterval() {
    for (int i=1 ; i <= NumofIntervals; ++i){
         Adder adder = new Adder(startRange,i == NumofIntervals ? endNum: endRange);
         AdderRunnable adderRunnable = new AdderRunnable(adder);
         Thread thread = new Thread(adderRunnable);
         maps.put(thread,adderRunnable);
         thread.start();
        startRange=  endRange += 1;
        System.out.println("startRange = " + startRange);
        endRange += diffRange-1;
        System.out.println("endRange = " + endRange);

    }}
          public void endOfThread() throws InterruptedException {
          for (final Map.Entry<Thread, AdderRunnable> entry : maps.entrySet()) {
             Thread thread = entry.getKey();
             AdderRunnable adderRunnable = entry.getValue();
             thread.join();
             totalSum += adderRunnable.getValue();

          }

      }

      public int getTotalSum(){
          return  totalSum;
      }

}
