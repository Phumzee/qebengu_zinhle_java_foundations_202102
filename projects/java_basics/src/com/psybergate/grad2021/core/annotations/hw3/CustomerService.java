package com.psybergate.grad2021.core.annotations.hw3;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class CustomerService {
    public static void main(String[] args) {
        Customer customer = new Customer("123","dubs","cross",1234, 24);
        System.out.println("insert(customer) = " + insert(customer));

    }

    public static  Customer getCustomer(){
        Customer customer = new Customer("123","dubs","cross",1234, 24);
        return customer;
    }

    public void SaveCustomer(Customer customer){
     insert(customer);


    }


    public static String insert(Customer customer){
        String insertSql = "INSERT INTO CUSTOMER (";
        int counter = 0;
         Class  clazz = Customer.class;
         Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            System.out.println("field = " + field);
            Annotation[] annotations = field.getDeclaredAnnotations();

            for (Annotation annotation : annotations) {
                if (annotation instanceof DomainProperty){

                    insertSql = insertSql +  field.getName() ;
                }

    }
            ++counter;
            if (counter < fields.length-1){
                insertSql = insertSql + ",";
            }

        }
        insertSql = insertSql + ")"  + " " + getValues(customer);
    return insertSql;}


    public static String getValues(Customer customer){
      String values = "VALUES(";
      int counter = 0;
      Field[] fields = Customer.class.getDeclaredFields();
        for (Field field : fields) {
            if (field.getName() == "customerNum"){
                 values = values +"'" +customer.getCustomerNum()+"'";

            }
            if (field.getName() == "name"){
                values = values + "'"+ customer.getName()+"'";
            }
            if (field.getName() == "surname"){
                values = values + "'"+customer.getSurname()+"'";
            }
            if (field.getName() == "dateOfBirth"){
                values = values + customer.getDateOfBirth();
            }
            ++counter;
            if  (counter <  fields.length-1){
                values = values + ",";}}

                values = values + ")";






      return values;
    }}
