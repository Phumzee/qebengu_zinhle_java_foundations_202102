package ce1a.standard;

import ce1a.standard.DateFactory;

import java.io.InputStream;
import java.util.Properties;

public class DateFactoryLoader {

    public static DateFactory dateFactory;

    public static DateFactory getDateFactory() {
        if (dateFactory != null){
            return dateFactory;
        }
        try (InputStream is = DateFactory.class.getClassLoader().getResourceAsStream("ce1a/date.properties.txt")){
            System.out.println(is);
            Properties props = new Properties();
            props.load(is);
            String dateFadtoryclass = props.getProperty("date.factory");
            dateFactory =  (DateFactory)  Class.forName(dateFadtoryclass).newInstance();
            return  dateFactory;

        } catch (Exception e) {
            throw new RuntimeException("Error - loading date class dynamically");
        }
    }
}
