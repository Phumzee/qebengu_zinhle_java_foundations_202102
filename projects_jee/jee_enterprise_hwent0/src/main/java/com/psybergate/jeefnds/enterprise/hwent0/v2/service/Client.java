package com.psybergate.jeefnds.enterprise.hwent0.v2.service;

import com.psybergate.jeefnds.enterprise.hwent0.v2.entities.Audit;
import com.psybergate.jeefnds.enterprise.hwent0.v2.entities.Customer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.time.LocalDate;

public class Client {

  public static void main(String[] args) {

    Customer customer = new Customer(123, "Barney", "Purple", LocalDate.of(2201, 10, 12));
    Audit audit = new Audit("12", LocalDate.of(2012, 12, 31));
    String sqlCustomer =
        "Insert into Customer  values (" + customer.getCustomerId() + ",'" + customer.getFirstName() + "','" + customer
            .getLastName() + "','" + customer.getDateOfBirth() + "');";
    System.out.println(sqlCustomer);
    String sqlAudit = "Insert into audit  values ('" + audit.getAuditNum() + "','" + audit.getDate() + "');";
    System.out.println(sqlAudit);
    passQueryToDatabase(sqlCustomer);
    passQueryToDatabase(sqlAudit);
  }

  public static void passQueryToDatabase(String sql) {
    Connection databaseConnection = null;
    Statement executor = null;
    try {
      Class.forName("org.postgresql.Driver");

      databaseConnection = DriverManager
          .getConnection("jdbc:postgresql://localhost:5432/hwent0",
              "postgres", "admin");

      executor = databaseConnection.createStatement();
      executor.executeUpdate(sql);
      executor.close();
      databaseConnection.close();
    } catch (Exception e) {
      throw new RuntimeException("Failed to execute SQL statement/s.", e);
    }
  }
}
