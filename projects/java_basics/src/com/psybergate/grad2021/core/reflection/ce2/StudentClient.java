package com.psybergate.grad2021.core.reflection.ce2;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class StudentClient {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Class clazz = Student.class;
        clazz.newInstance();
        Constructor<Student> constructor1 = clazz.getConstructor(String.class,String.class,String.class);
        Constructor<Student> constructor2 = clazz.getConstructor();

        Student student1 = constructor2.newInstance();
        Student student2 = constructor1.newInstance("123", "Zee","Q");
       Student s = new Student();
        Method method = clazz.getDeclaredMethod("isStudent");
        System.out.println("method.invoke(s) = " + method.invoke(s));


    }
}
