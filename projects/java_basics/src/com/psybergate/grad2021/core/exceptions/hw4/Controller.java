package com.psybergate.grad2021.core.exceptions.hw4;

public class Controller {



    public static void doAddAccount(String accountNum, String accountHolderName, double balance) {

        try {
            if (AccountValidator.accountExist(accountNum)) { //account already exits
                System.out.println("The account with the similar accountNum already exists, Please pick a different accountNum");
            }
        } catch (AccountAlreadyExistsException e) {
            Service.doAddAccount(accountNum, accountHolderName, balance);
        }
    }

    public static void doDeleteAccount(String accountNum) {
        try {
            if (AccountValidator.accountExist(accountNum)) {
                Service.doDeleteAccount(accountNum);
            }

        } catch (AccountAlreadyExistsException e) {
            System.out.println("Cannot delete a non-existing account");
       } }

    public static void doDeposit(String accountNum, double amount) throws AccountAlreadyExistsException {
        if (AccountValidator.accountExist(accountNum)) {
            Service.doDeposit(accountNum, amount);
        }
    }

    public static void doWithdrawal(String accountNum, double amount) throws AccountAlreadyExistsException {
        if (AccountValidator.accountExist(accountNum)) {
            Service.doWithdrawal(accountNum, amount);
        }
    }

    public static void printAccountDetails(String accountNum){
        try{
            if(AccountValidator.accountExist(accountNum)){
                Service.printAccountDetails(accountNum);
            }
        } catch ( AccountAlreadyExistsException e) {
            System.out.println(("Cannot print account details on a non existing account"));
        }

}}
