package ce3.client;

import ce3.standards.CommandEngine;
import ce3.standards.CommandRequest;
import ce3.vendorImpl1.CommandEngineImpl1;
import ce3.vendorImpl1.CommandRequestImpl;

public class Client {

    public static void main(String[] args) {

        CommandRequest commandRequest = new CommandRequestImpl(new Sum(1,2));
        CommandEngine commandEngine = new CommandEngineImpl1(commandRequest);
        commandEngine.start();
    }
}
