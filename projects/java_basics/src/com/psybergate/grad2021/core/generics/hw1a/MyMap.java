package com.psybergate.grad2021.core.generics.hw1a;

public class MyMap {
    private Object  key;
    private Object  value;

    public MyMap(Object key, Object value){
       this.key = key;
       this.value = value;

    }

    public Object getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
