package com.psybergate.java.exceptions.hw2;

import java.sql.SQLException;
import java.util.Scanner;

public class ClassA {

  public static void main(String[] args) {
    try {
      methodA();
    } catch (ApplicationException e) {
      e.printStackTrace();
    }
  }

  public static void methodA() throws ApplicationException{

    try {
      methodB();
    } catch (SQLException throwables) {
      throwables.getMessage();
      throwables.printStackTrace();

    }
    throw new ApplicationException();



  }

  public static  void methodB() throws SQLException {
    Scanner in = new Scanner(System.in);
    String s = in.nextLine();
    if (s == "abc"){
      throw new SQLException("This is a SQL Exception");
    }

  }

}
