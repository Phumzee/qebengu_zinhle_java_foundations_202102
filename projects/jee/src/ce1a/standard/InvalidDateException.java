package ce1a.standard;

public class InvalidDateException extends Exception {

    public static final int DAY_INVALID = 1;

    public static final int MONTH_INVALID = 2;

    public static final int YEAR_INVALID = 3;
  

    private  int dateErrorCode;

    public InvalidDateException(int dateErrorCode, String message) {
        super(message);
        this.dateErrorCode = dateErrorCode;
    }

    public int getDateErrorCode() {
        return dateErrorCode;
    }
}
