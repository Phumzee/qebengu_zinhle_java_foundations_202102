package com.psybergate.core.collections.ce1.v1;

import java.util.Collection;
import java.util.Iterator;

public abstract class ArrayStack implements Stack {

  public static void main(String[] args) {
//    ArrayStack as1 = new ArrayStack();
//    as1.push(new Object("abc1"));
//    as1.push(new Object("abc2"));
//    as1.push(new Object("abc3"));
//    as1.print();


  }


  Object bottom;


  public void push(Object o) {
    if (bottom == null) {
      bottom = o;
    } else {
      Object focus = bottom;
      while (focus.next != null) {
        focus = focus.next;
      }
      focus.next = o;
      add(o);
    }
  }

  @Override
  public Object pop() {
    Object focus = bottom;
    while (focus.next != null) {
      focus = focus.next;
    }
    return focus.next;
  }

  @Override
  public Object get(int position) {
    int icount = 0;
    if (position == 0) {
      return bottom;
    } else {
      Object focus = bottom;
      while (icount < position) {
        focus = focus.next;
        ++icount;
      }
      contains(focus);
      return focus;
    }
  }


  @Override
  public int size() {
    return 0;
  }

  @Override
  public boolean isEmpty() {
    if (bottom == null){
      return true;
    }
    return false;
  }

  public void print(){
    Object focus = bottom;
    while (focus.next != null){
    System.out.println(focus);
    focus = focus.next;
    }

  }


  @Override
  public boolean contains(java.lang.Object o) {
    if (o != null){
      return true;
    }
    return false;
  }


  @Override
  public Iterator iterator() {
    return null;
  }

  @Override
  public java.lang.Object[] toArray() {
    return new java.lang.Object[0];
  }

  @Override
  public boolean add(java.lang.Object o) {
    if (o != null ){
      return true;
    }
    return false;
  }

  @Override
  public boolean remove(java.lang.Object o) {
    return false;
  }

  @Override
  public boolean addAll(Collection c) {
    return false;
  }

  @Override
  public void clear() {

  }

  @Override
  public boolean retainAll(Collection c) {
    return false;
  }

  @Override
  public boolean removeAll(Collection c) {
    return false;
  }

  @Override
  public boolean containsAll(Collection c) {
    return false;
  }

  @Override
  public java.lang.Object[] toArray(java.lang.Object[] a) {
    return new java.lang.Object[0];
  }
}

