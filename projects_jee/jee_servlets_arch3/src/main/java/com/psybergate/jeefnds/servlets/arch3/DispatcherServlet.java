package com.psybergate.jeefnds.servlets.arch3;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@WebServlet("/dispatch/*")
public class DispatcherServlet extends HttpServlet {


    private static final Controller GreetingController = new GreetingController();
    private static final Controller DateController = new DateController();

    private static final Map<String, Controller> CONTROLLERS = new HashMap<>();

    public void init(){
        loadControllers();
    }


    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String reqpathInfo = req.getPathInfo();
        Controller controller = getController(reqpathInfo);
        String methodStr = reqpathInfo.substring(1);
        Method method = null;
        try {
            method = controller.getClass().getMethod(methodStr);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        try {
            method.invoke(controller, req, resp);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

    }


    public static void loadControllers() {
        try{
            InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("controller.properties");
            Properties props = new Properties();
            props.load(is);
            for (Map.Entry entry : props.entrySet()){
                Controller controller = (Controller) Class.forName(entry.getValue().toString()).newInstance();
                CONTROLLERS.put(entry.getKey().toString(),controller);

            }
        } catch (Exception ex) {
            throw new RuntimeException("Error - Exception not handled", ex);
        }

    }

    private Controller getController(String reqpathInfo){
        return CONTROLLERS.get(reqpathInfo);
    }


}

