package com.psybergate.java.langoverview.oo_basics.ce4a1;

public class CurrentAccount extends  Account {
    private double interestRate;
    private double overdraft;

    public CurrentAccount(String accountNum, double balance, double interestRate, double overdraft){
        super(accountNum, balance);
        this.interestRate = interestRate;
        this.overdraft = overdraft;
    }

    public void print(){
        System.out.println("This is a Current Account.");
    }




}
