package com.psybergate.grad2021.core.concurrency.hw1.v2;

public class MessageWriterRunnable implements  Runnable{

    private MessageWriter messageWriter;

    public MessageWriterRunnable(MessageWriter messageWriter) {
        this.messageWriter = messageWriter;
    }

    @Override
    public void run() {
      messageWriter.write();
    }
}
