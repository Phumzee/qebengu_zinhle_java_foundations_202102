package com.psybergate.jeefnds.servlets.hwweb5_v2;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.annotation.WebServlet;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@WebListener("helloworld")
public class MyServletContextListener implements ServletContextListener {

    @Override

    public void contextInitialized(ServletContextEvent servletContextEvent) {

        System.out.println("entering contextInit()...");

        ServletContext sc = servletContextEvent.getServletContext();


        List<String> names = Arrays.asList("zinhle", "pearcy", "Noxy", "Pearl", "Felicia");

         Random random = new Random();
         String name = names.get(random.nextInt(5));
        sc.setAttribute("name", name);


    }


    @Override

    public void contextDestroyed(ServletContextEvent servletContextEvent) {


    }

}



