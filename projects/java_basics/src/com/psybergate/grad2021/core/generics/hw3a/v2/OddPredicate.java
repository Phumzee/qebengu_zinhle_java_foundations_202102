package com.psybergate.grad2021.core.generics.hw3a.v2;

import java.util.Arrays;
import java.util.Collection;

public class OddPredicate implements UnaryPredicate<Integer>{

   public static void main(String[] args) {
       Collection<Integer> ci = Arrays.asList(1,2,3,4);
       int count = Algorithm.countIf(ci, new OddPredicate());
       System.out.println("Number of odd integers="+ count);

    }
    @Override
    public boolean test(Integer i) {
        return i % 2 != 0;
    }
}
