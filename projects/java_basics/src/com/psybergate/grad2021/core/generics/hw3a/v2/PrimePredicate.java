package com.psybergate.grad2021.core.generics.hw3a.v2;

import java.util.Arrays;
import java.util.Collection;

public class PrimePredicate implements UnaryPredicate<Integer>{
    public static void main(String[] args) {
        Collection<Integer> ci = Arrays.asList(1,2,3,4);
        int count = Algorithm.countIf(ci, new PrimePredicate());
        System.out.println("Number of odd integers="+ count);

    }


    @Override
    public boolean test(Integer obj) {
        return isPrime(obj);
    }

    public static boolean isPrime(int number) {
        int sqrt = (int) Math.sqrt(number) + 1;
        for (int i = 2; i < sqrt; i++) {
            if (number % i == 0) {
                return false; } }
        return true; }}


