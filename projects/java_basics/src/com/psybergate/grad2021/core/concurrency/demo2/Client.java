package com.psybergate.grad2021.core.concurrency.demo2;

public class Client {
    public static void main(String[] args) {

        Iterator i2 = new Iterator(10);
        Iterator i1 = new Iterator(15);
        ThreadRunnable threadRunnable1 = new ThreadRunnable(i1);
        ThreadRunnable threadRunnable2 = new ThreadRunnable(i2);
        Thread thread1 = new Thread(threadRunnable1);
        Thread thread2 = new Thread(threadRunnable2);
        thread1.start();
        thread2.start();
    }
}
