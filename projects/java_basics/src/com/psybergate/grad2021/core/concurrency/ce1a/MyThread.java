package com.psybergate.grad2021.core.concurrency.ce1a;

import java.util.zip.CheckedInputStream;

public class MyThread extends Thread {


    public MyThread() {
    }

    public void run(){
      System.out.print("Program1 is running");
      doSomething1();
  }

    private void doSomething1() {
        doSomething2();

    }

    private void doSomething2() {
        throw new RuntimeException("Thread is running.");
    }
}
