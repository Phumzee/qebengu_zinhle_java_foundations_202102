package com.psybergate.grad2021.core.concurrency.hw1.v3;


public class MessageReaderRunnable implements Runnable{

  private MessageReader messageReader;

  public MessageReaderRunnable(MessageReader messageReader) {
    this.messageReader = messageReader;
  }

  @Override
    public void run() {
//    try {
//      Thread.sleep(1000);
//    } catch (InterruptedException e) {
//      e.printStackTrace();
//    }
    messageReader.read();
    }
}
