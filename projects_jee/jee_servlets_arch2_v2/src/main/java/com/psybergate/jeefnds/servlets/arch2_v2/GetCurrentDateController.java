package com.psybergate.jeefnds.servlets.arch2_v2;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;

public class GetCurrentDateController implements Controller {


    @Override
    public String execute(HttpServletRequest req) {
        LocalDate date = LocalDate.now();
        return "" + date;
    }
}
