package ce1a.standard;

public interface Date {

    int getYear();

    int getMonth();

    int getDay();

    /**
     * Returns true if current date is a leap year
     */

    boolean isLeapYear();

    Date addDays(int numDays) throws InvalidDateException;


}
