package com.psybergate.grad2021.core.reflection.hw3;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;

import static java.lang.invoke.MethodHandles.lookup;

public class MethodHandle1 {

    public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException {
        final MethodHandles.Lookup lookup = lookup();
        MethodHandle isStudent = lookup.findGetter(Class.forName("com.psybergate.grad2021.core.reflection.hw3.Student"), "studentNum", String.class);
        MethodHandle isStudent2 = lookup.findGetter(Class.forName("com.psybergate.grad2021.core.reflection.hw3.Student"), "name", String.class);
        MethodHandle isStudent3 = lookup.findGetter(Class.forName("com.psybergate.grad2021.core.reflection.hw3.Student"), "surname", String.class);
       // MethodHandle isStudent = lookup.findGetter(Class.forName("com.psybergate.grad2021.core.reflection.hw3.Student"), "studentNum", String.class);
        System.out.println("isStudent = " + isStudent);
       // MethodType methodType1 = lookup.findConstructor();
        //MethodType methodType2 = lookup.findStatic();
    }




}
