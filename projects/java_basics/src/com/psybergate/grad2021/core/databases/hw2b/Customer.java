package com.psybergate.grad2021.core.databases.hw2b;

import hw3a.v1.Account;

import java.util.ArrayList;
import java.util.List;

public class Customer {
    private String customerNum;
    private String name;
    List<Account> acc = new ArrayList<>();


    public Customer(String customerNum, String name) {
        this.customerNum = customerNum;
        this.name = name;
    }

    public String getCustomerNum() {
        return customerNum;
    }

    public String getName() {
        return name;
    }

    public List<Account> getAcc() {
        return acc;
    }

    public void addAcc(Account a){
        acc.add(a);
    }
}
