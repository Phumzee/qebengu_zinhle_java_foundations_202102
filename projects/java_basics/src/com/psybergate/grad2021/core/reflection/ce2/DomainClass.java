package com.psybergate.grad2021.core.reflection.ce2;

@interface DomainClass {

    String name() default "";
}
