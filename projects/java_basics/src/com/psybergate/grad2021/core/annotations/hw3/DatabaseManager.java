package com.psybergate.grad2021.core.annotations.hw3;




import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.*;

public class DatabaseManager {
    public static void main(String[] args) throws SQLException {
     connectDB();
        System.out.println("createSql() = " + createSql());


    }


    public static void connectDB() throws SQLException {
        Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
                            "postgres", "admin");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Opened database successfully");
       Statement statement = connection.createStatement();
      String sql = CustomerService.insert(CustomerService.getCustomer());
       statement.executeUpdate(sql);
       statement.close();
       connection.close();


    }

    public static String createSql(){
      String createSql = "CREATE TABLE CUSTOMER("  ;
      Class clazz = Customer.class;
      Annotation[] annotations2 = clazz.getAnnotations();
        System.out.println("annotations2.length = " + annotations2.length);
          int counter = 0;
      Field[] fields = clazz.getDeclaredFields();
        System.out.println("fields[fields.length-2] = " + fields[fields.length - 2]);
        for (Field field : fields) {
            System.out.println("field = " + field);
            Annotation[] annotations = field.getDeclaredAnnotations();

            for (Annotation annotation : annotations) {
                //System.out.println("annotation = " + annotation);
                if (annotation instanceof DomainProperty){
                    String constraints = "";
                    if (((DomainProperty) annotation).Primarykey() == true){
                        constraints = constraints + "PRIMARY KEY ";
                    }
                    if (((DomainProperty) annotation).isNull() == false && ((DomainProperty) annotation).Primarykey()==false ){
                        constraints = constraints + "NOT NULL";
                    }

                    createSql = createSql +  field.getName() + " " + gettype(field) +" " + constraints;
                }
            }
            ++counter;
          if (counter < fields.length-1){
              createSql = createSql + ",";
          }

        }
         createSql = createSql + ")";
      return  createSql;
    }


    public static String gettype(Field field){
        String type = "";
      if (field.getType().equals(String.class)){
          type = "TEXT";
      }
      if (field.getType().equals(int.class)){
          type = "INT";
      }
      if (field.getType().equals(Integer.class)){
          type = "INT";
      }


       return type;
    }
}
