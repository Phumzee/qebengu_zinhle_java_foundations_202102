package com.psybergate.core.collections.hw1;

import java.util.Comparator;

public class IncreasingOrder implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        Customer e1 = (Customer) o1;
        Customer e2 = (Customer) o2;
        return e1.getCustomerNum() - e2.getCustomerNum();

    }
}
