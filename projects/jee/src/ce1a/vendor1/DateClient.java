package ce1a.vendor1;

import ce1a.standard.Date;
import ce1a.standard.DateFactory;
import ce1a.standard.DateFactoryLoader;
import ce1a.standard.InvalidDateException;

public class DateClient {

    public static void usesDates() throws InvalidDateException {

        DateFactory df = DateFactoryLoader.getDateFactory();
        Date d = df.createDate(10,10,2001);

        System.out.println("d.getMonth() = " + d.getMonth());
        System.out.println("df.getClass() = " + df.getClass());
        System.out.println("d.getClass() = " + d.getClass());
        System.out.println("d.addDays(4) = " + d.addDays(4));


    }

    public static void main(String[] args) throws InvalidDateException {
        usesDates();
    }

}
