package com.psybergate.jeefnds.enterprise.hwent3.framework;

import javax.enterprise.inject.spi.CDI;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Properties;

@WebServlet("/customerapp/*")
public class DispatcherServlet extends HttpServlet {
  private Properties properties = new Properties();

  @Override
  public void init() throws ServletException {

    InputStream stream = DispatcherServlet.class.getClassLoader().getResourceAsStream("app.properties");
    try {
      properties.load(stream);
    } catch (IOException e) {
      throw new RuntimeException("Couldn't load properties!");
    }
  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response) {
    String pathInfo = request.getPathInfo().substring(1);

    try {

      String classAndMethod = properties.getProperty(pathInfo);
      int index = classAndMethod.indexOf('#');
      String className = classAndMethod.substring(0, index);
      String methodName = classAndMethod.substring(index + 1);
      Object controller = CDI.current().select(Class.forName(className)).get();
      Method method = controller.getClass().getMethod(methodName, HttpServletRequest.class, HttpServletResponse.class);
      method.invoke(controller, request, response);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }

  }

}

