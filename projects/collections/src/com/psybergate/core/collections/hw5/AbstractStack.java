package com.psybergate.core.collections.hw5;

import com.psybergate.core.collections.ce1.v2.Stack;

import java.util.Collection;
import java.util.Iterator;

public abstract class AbstractStack implements Stack {


    @Override
    public abstract int size();

    @Override
    public abstract boolean isEmpty() ;
    @Override
    public abstract boolean contains(Object o);

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public abstract boolean add(Object o);

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }



    }

