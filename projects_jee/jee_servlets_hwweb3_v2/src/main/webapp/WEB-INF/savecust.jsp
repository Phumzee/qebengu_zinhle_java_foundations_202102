<!DOCTYPE html>
<html>
<head>
<title>Insert Data</title>
</head>
<body>
    <!-- Give Servlet reference to the form as an instances 
    GET and POST services can be according to the problem statement-->
    <form action="<%= request.getContextPath() %>/dispatch/customer/add" method="post">
        <p>CustomerNum:</p>
        <!-- Create an element with mandatory name attribute,
        so that data can be transfer to the servlet using getParameter() -->
        <input type="long" name="customernum"/>
        <br/>
        <p>Name:</p>
        <input type="text" name="name"/>
        <br/><br/><br/>
        <p>Surname:</p>
        <input type="text" name="surname"/>
        <br/><br/><br/>
        <p>Date of Birth:</p>
        <input type="date" name="dateofbirth"/>
        <br/><br/><br/>
        <input type="submit"/>
    </form>
</body>
</html>