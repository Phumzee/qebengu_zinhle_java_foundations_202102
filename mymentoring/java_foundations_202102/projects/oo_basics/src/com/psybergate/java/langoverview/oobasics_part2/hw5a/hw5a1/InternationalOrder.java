package com.psybergate.java.langoverview.oobasics_part2.hw5a.hw5a1;

public class InternationalOrder extends Order {

    private double importDuties;
    private static final double MORE_THAN_HALFMILLI = 0.05;
     private static final double MORE_THAN_MILLI = 0.1;
     private double internationalDisocunt;

    public InternationalOrder(double importDuties) {
        this.importDuties = importDuties;
    }


    public double getDiscount(){
        if (super.getOrderTotal() > 500000){
            internationalDisocunt = MORE_THAN_HALFMILLI;
        }else if (super.getOrderTotal()> 1000000){
            internationalDisocunt = MORE_THAN_HALFMILLI;
        }
        return internationalDisocunt;
    }


    public double getOrderTotal() {
        getDiscount();
        return (super.getOrderTotal()  * (1 - internationalDisocunt))+ importDuties;

    }
}


