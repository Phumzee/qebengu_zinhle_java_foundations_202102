package com.psybergate.java.langoverview.oobasics_part2.ce1a;

import com.psybergate.java.langoverview.oobasics_part2.ce1a.Account;

public class CurrentAccount extends Account {

    private static final String ACCOUNT_TYPE   = "Current Account";
    private double overdraft;

  //how I fixed code 1:
    public CurrentAccount(String accountNum){
      super(accountNum);
    }



   public CurrentAccount(String accountNum, double balance) {
        super(accountNum, balance);
       // this.overdraft = overdraft;
       System.out.println("Hey I am a current account.");


    }

   // Code 3:
    //if I only had this constructor in CurrentAccount ; these would not compile
    // Account a1 = new CurrentAccount("1234");
   //        Account a2 = new CurrentAccount("1234",12000);
    public CurrentAccount(String accountNum, double balance, double overdraft){
        super(accountNum, balance);
        this.overdraft = overdraft;
    }



}
