package ce3.client;



import ce3.standards.Command;
import ce3.standards.CommandRequest;
import ce3.standards.CommandResponse;

import java.time.LocalDate;

public class CurrentDate implements Command {

    CommandResponse commandResponse;

    public void  execute(){

        System.out.println("LocalDate.now() = " + LocalDate.now());

    }


}
