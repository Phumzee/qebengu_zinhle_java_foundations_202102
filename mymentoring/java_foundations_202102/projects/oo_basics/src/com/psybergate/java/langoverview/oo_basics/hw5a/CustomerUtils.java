package com.psybergate.java.langoverview.oo_basics.hw5a;
import  java.util.*;
public class CustomerUtils {
    public static void main(String[] args) {
        Customer c1 = new Customer("123");

        Account a1 = new CurrentAccount("123",12000,10000);
        Account a2 = new CurrentAccount("123", 10000, 5000);

        c1.addAccount(a1);
        c1.addAccount(a2);
        c1.printBalances();



    }


}
