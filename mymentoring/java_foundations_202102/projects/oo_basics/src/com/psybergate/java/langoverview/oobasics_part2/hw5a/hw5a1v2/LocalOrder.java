package com.psybergate.java.langoverview.oobasics_part2.hw5a.hw5a1v2;

public class LocalOrder extends Order {

    private static final double LOCAL_DISCOUNT = 0.10;
    private static final double TWO_YEAR_DISCOUNT = 0.075;
    private static final double FIVE_YEAR_DISCOUNT = 0.125;

  //  private static double localDiscount;

    public LocalOrder(int orderId, Customer customer, String orderDate) {

        super(orderId, customer, orderDate);
    }


    public double getLocalDiscount(Customer c){
      double localDiscount = 0;
       if (c.getCustomerDuration() >=2 && c.getCustomerDuration() <=5){
           localDiscount = TWO_YEAR_DISCOUNT;
       }else if (c.getCustomerDuration() > 5){
           localDiscount = FIVE_YEAR_DISCOUNT;
       }
      return localDiscount;
    }


    public  double getOrderTotal(){
       return super.getOrderTotal() * (1 - getLocalDiscount(this.customer));


    }

}
