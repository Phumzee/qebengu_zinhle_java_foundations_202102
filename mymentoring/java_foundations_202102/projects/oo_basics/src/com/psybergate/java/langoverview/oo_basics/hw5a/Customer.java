package com.psybergate.java.langoverview.oo_basics.hw5a;

import java.util.ArrayList;

public class Customer {
    private String customerNum;

   private  ArrayList<Account>accounts = new ArrayList<>();

    public  ArrayList<Account> getAccounts() {
        return accounts;
    }

    public Customer(String customerNum){
        this.customerNum = customerNum;
    }
    public String getCustomerNum(){
        return customerNum;
    }



    public void addAccount(Account a){
        accounts.add(a);
    }


    public void setAccounts(ArrayList<Account> accounts) {
        this.accounts = accounts;
    }

    public   void  printBalances() {
        System.out.println("Customer num: " + customerNum);
        for (int i=0 ; i < accounts.size(); ++i) {
            System.out.println(getAccounts().get(i).getAccountNum() + " " + getAccounts().get(i).getBalance());
        }
} }
