package com.psybergate.grad2021.core.concurrency.hw2;

public class VolatileData {
    private volatile int counter = 0;

    public int getCounter(){
        return counter;
    }

    public int increaseCounter(){
       return  ++counter;

    }
}
