package com.psybergate.grad2021.core.concurrency.hw1.v3;

import com.psybergate.grad2021.core.concurrency.hw1.v2.MessageCache;

import java.util.Random;

public class MessageWriter {

    private MessageCache messageCache = MessageCache.getINSTANCE();

    public  void write()  {
        int i=0;
        do{
        Random random = new Random();
        String s = "Random String: " + random.nextInt(1000);
        System.out.println("Writing...." + s);
        messageCache.add(s);
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }


            ++i;}
        while (i < 5);

    }

}
