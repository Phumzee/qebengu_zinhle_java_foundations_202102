package com.psybergate.jeefnds.servlets.arch2;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/*")
public class DispatcherServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {



        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><body>");

        String param = req.getParameter("controller");
        String name= req.getParameter("name");
        if (param.equals("getCurrentDate")){
            out.println(GetCurrentDateController.getDate());
        }else {
            out.println(HelloWorldController.getString() + name);
        }

//        if (req.getPathInfo().equals("getCurrentDate")){
//            out.println(GetCurrentDateController.getDate());
//
//        }

        out.println("<html><body>");
        out.close();
    }
}
