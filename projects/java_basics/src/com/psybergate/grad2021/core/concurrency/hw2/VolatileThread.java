package com.psybergate.grad2021.core.concurrency.hw2;

public class VolatileThread implements  Runnable{

    private final VolatileData volatileData;

    public VolatileThread(VolatileData volatileData) {
        this.volatileData = volatileData;
    }

    @Override
    public void run() {
        int oldValue = volatileData.getCounter();
       //int value =  volatileData.increaseCounter();
        System.out.println("[Thread" + Thread.currentThread().getId() + "]: Old value=" +
                volatileData.increaseCounter());
        int newValue = volatileData.getCounter();
        System.out.println("[Thread" + Thread.currentThread().getId()+ "]: New value=" + newValue);
    }
}
