package com.psybergate.grad2021.core.concurrency.hw1.v3;


public class MessageReader {

    private MessageCache messageCache = MessageCache.getINSTANCE();


    public void read() {
            System.out.println("messageCache.read() = " + messageCache.read());
            messageCache.getMessages().remove(0);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        }
    }

