package com.psybergate.pri.acces.modifiers.person;

public class Person {
    protected String name;
    protected String dob;
    protected  int age;

    public Person(String name, String dob, int age){
        this.name = name;
        this.dob = dob;
        this.age = age;
    }


    protected String getName(){
        return name;
    }

    public int returnAge(){
        return age;
    }



}
