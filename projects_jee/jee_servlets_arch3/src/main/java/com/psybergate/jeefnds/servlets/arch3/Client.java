package com.psybergate.jeefnds.servlets.arch3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Client {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        DateController o = new DateController();
        Class clazz = DateController.class;
        String s1 =  o.getClass().getMethod("getCurrentDate").invoke(o).toString();
        System.out.println("s1 = " + s1);
    }
}
