package com.psybergate.grad2021.core.innerclasses.hw1;

public class StaticInnerClass {
    static class NestedDemo {
        public void anyMethod(){
            System.out.println("This is my nested class");
        }
    }

    public static void main(String[] args) {
        StaticInnerClass.NestedDemo nesty = new NestedDemo();
        nesty.anyMethod();
    }
}
