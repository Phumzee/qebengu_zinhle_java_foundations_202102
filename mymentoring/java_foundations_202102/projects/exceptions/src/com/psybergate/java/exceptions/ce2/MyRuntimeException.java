package com.psybergate.java.exceptions.ce2;

public class MyRuntimeException extends  RuntimeException{

  public MyRuntimeException() {
  }

  public MyRuntimeException(String message) {
    super(message);
  }
}
