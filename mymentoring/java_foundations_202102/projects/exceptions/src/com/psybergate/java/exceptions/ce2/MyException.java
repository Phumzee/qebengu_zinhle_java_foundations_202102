package com.psybergate.java.exceptions.ce2;

public class MyException extends Exception{

  public MyException(String message, Throwable cause) {
    super(message, cause);
  }

  public MyException(Throwable cause) {
    super(cause);
  }

  public MyException() {
    System.out.println("This is a Checked Exception.");
  }

  public MyException(String message) {
    super(message);
  }
}
