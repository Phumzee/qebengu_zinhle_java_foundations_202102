package com.psybergate.jeefnds.servlets.arch2_v2;

import javax.servlet.http.HttpServletRequest;

public class HelloWorldController implements Controller {


    @Override
    public String execute(HttpServletRequest req) {
        String string = "Hello " + req;
        return string;
    }
}
