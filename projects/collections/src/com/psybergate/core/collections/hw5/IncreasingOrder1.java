package com.psybergate.core.collections.hw5;

import com.psybergate.core.collections.hw1.Customer;

import java.util.Comparator;

public class IncreasingOrder1 implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        String s1 = (String) o1;
        String s2 = (String) o2;
        return s1.compareTo(s2);

    }
}
