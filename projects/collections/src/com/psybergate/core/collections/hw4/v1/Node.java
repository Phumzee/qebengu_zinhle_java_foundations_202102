package com.psybergate.core.collections.hw4.v1;

public class Node {

    public Object s1;
    public Object next = null;

    public Node(Object s1) {
        this.s1 = s1;
    }

    @Override
    public String toString() {
        return "Node{" +
                "s1='" + s1 + '\'' +
                ", next=" + next +
                '}';
    }
}
