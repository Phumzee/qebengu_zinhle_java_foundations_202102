package com.psybergate.grad2021.core.annotations.hw1;

 @interface DomainTransient {
    boolean isNull() default true;

    boolean PrimaryKey() default false;
}
