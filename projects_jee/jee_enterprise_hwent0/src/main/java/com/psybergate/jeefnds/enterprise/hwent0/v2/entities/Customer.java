package com.psybergate.jeefnds.enterprise.hwent0.v2.entities;

import javax.persistence.Table;
import java.time.LocalDate;

@Table
public class Customer {

  private long customerId;

  private String firstName;

  private String lastName;

  private LocalDate dateOfBirth;

  public Customer(long customerId, String firstName, String lastName, LocalDate dateOfBirth) {
    this.customerId = customerId;
    this.firstName = firstName;
    this.lastName = lastName;
    this.dateOfBirth = dateOfBirth;
  }

  public long getCustomerId() {
    return customerId;
  }

  public void setCustomerId(long customerId) {
    this.customerId = customerId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public LocalDate getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(LocalDate dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }
}
