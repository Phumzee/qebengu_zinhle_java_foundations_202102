package com.psybergate.grad2021.core.concurrency.ce4a;

import sun.plugin2.message.Message;

public class Client {
    private static Object Message;

    public static void main(String[] args) {
    Singleton singleton = new Singleton();
    SingletonRunnable singletonRunnable = new SingletonRunnable(singleton);
    Thread thread1 = new Thread(singletonRunnable);
    Thread thread2 = new Thread(singletonRunnable);
    thread1.start();
    thread2.start();


    }
}
