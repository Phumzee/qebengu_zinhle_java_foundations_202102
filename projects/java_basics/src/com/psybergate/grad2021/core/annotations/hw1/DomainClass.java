package com.psybergate.grad2021.core.annotations.hw1;

@interface DomainClass {

    String name() default "";
}
