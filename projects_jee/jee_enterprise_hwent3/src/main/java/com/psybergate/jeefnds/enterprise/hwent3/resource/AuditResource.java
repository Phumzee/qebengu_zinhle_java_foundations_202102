package com.psybergate.jeefnds.enterprise.hwent3.resource;

import com.psybergate.jeefnds.enterprise.hwent3.entity.Audit;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@ApplicationScoped
public class AuditResource {

  @PersistenceContext
  private EntityManager entityManager;

  public void save(Audit audit) {
    entityManager.persist(audit);
  }
}
