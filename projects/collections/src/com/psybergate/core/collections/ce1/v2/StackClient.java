package com.psybergate.core.collections.ce1.v2;

public class StackClient {
    public static void main(String[] args) {
        Stack stack = new ArrayStack();
        System.out.println("stack.isEmpty() = " + stack.isEmpty());
        Object abc1 = "abc1";
        Object abc2 = "abc2";
        Object abc3 = "abc3";
        Object abc4 = "abc4";
        stack.push(abc1);
        stack.push(abc2);
        stack.push(abc3);
        stack.push(abc4);
        System.out.println(stack.contains(abc1));
        System.out.println(stack.contains(abc3));
        System.out.println("stack.isEmpty() = " + stack.isEmpty());
        System.out.println("stack.get(0) = " + stack.get(0));
        System.out.println("stack.get(i) = " + stack.get(1));


        System.out.println("stack.pop() = " + stack.pop());
        System.out.println("stack.pop() = " + stack.pop());
        System.out.println("stack.pop() = " + stack.pop());
        System.out.println("stack.pop() = " + stack.pop());
        //System.out.println("stack.pop() = " + stack.pop());


    }

}
