package com.psybergate.java.langoverview.oo_basics.ce1a;

import org.omg.CORBA.Current;

import java.util.*;
public class Customer {


    private int customerNum;
    private String name;
    private String address;
    private ArrayList<CurrentAccount>ca = new ArrayList<>();



   public  Customer (int customerNum, String name, String address){
       this.customerNum = customerNum;
       this.name = name;
       this.address = address;
   }

   public void addAccount (CurrentAccount c){
       ca.add(c);
   }

    public double getTotalBalance() {
        double balance = 0;
        for (int i=0; i < ca.size(); ++i) {
            balance += ca.get(i).getBalance();
        }

        return balance;

    }


}
