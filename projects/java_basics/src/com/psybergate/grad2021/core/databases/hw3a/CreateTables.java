package com.psybergate.grad2021.core.databases.hw3a;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class CreateTables {
    public static void main(String[] args) throws IOException {
        create();
    }

    public static void create() throws IOException {
        String sql = "";
        sql = sql + createAccountType() + createTransactionType() + createCustomer() + createAccount() + createTransaction();
        System.out.println("sql = " + sql);
        FileWriter myWriter = new FileWriter("filename.sql");
        myWriter.write(sql);
        myWriter.close();

    }

    public static String createCustomer(){
        String sql = "CREATE TABLE IF NOT EXISTS CUSTOMER (customerNum TEXT PRIMARY KEY, name TEXT, surname TEXT, dateOfBirth TEXT," ;
        sql+= "address TEXT); ";

        return sql;

    }

    public static String createAccount(){
        String sql = "CREATE TABLE IF NOT EXISTS ACCOUNT(accountNum TEXT PRIMARY KEY, balance FLOAT , customerNum TEXT,";
        sql = sql + "accountTypeID TEXT, FOREIGN KEY (customerNum) REFERENCES Customer(customerNum),";
        sql = sql + "FOREIGN KEY (accountTypeID) REFERENCES AccountType(accountTypeID));";
        return sql;
    }

    public static  String createAccountType(){
        String sql = "CREATE TABLE IF NOT EXISTS ACCOUNTTYPE(accountTypeId TEXT PRIMARY KEY , description TEXT);";
        return sql;
    }

    public static String createTransaction(){
        String sql = "CREATE TABLE IF NOT EXISTS TRANSACTION(transactionNum TEXT PRIMARY KEY, transactionTypeID TEXT,";
        sql = sql + " accountNum TEXT, FOREIGN KEY (accountNum) REFERENCES account(accountNum),";
        sql = sql + "FOREIGN KEY (transactionTypeID) references TransactionType(transactionTypeID));";
        return sql;
    }

    public static String createTransactionType(){
        String sql = "CREATE TABLE IF NOT EXISTS TRANSACTIONTYPE(transactionTypeId TEXT PRIMARY KEY , description TEXT);";
        return sql;
    }






}
