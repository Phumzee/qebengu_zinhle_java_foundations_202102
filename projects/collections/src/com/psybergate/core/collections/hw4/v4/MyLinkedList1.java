package com.psybergate.core.collections.hw4.v4;

//import com.psybergate.core.collections.hw4.v2.Object;

import com.psybergate.core.collections.hw4.v4.Node;

import java.util.*;

public class MyLinkedList1 implements List {


    private Node head;


    @Override
    public int size() {
       int i;
        Node focusNode = new Node(null);
        focusNode.next = head;
        for ( i=0; focusNode.next != null; ++i) {
            focusNode = focusNode.next;
        }

        return i;
    }


    @Override
    public boolean isEmpty() {
        if (size() == 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean contains(Object o) {
        int size = 0;
        if (isEmpty()){
            return false;
        }else {
        Node focusNode = head;
        for (int i = 0; i < size(); ++i) {
            if (focusNode == o) {
                return true;
            }
            focusNode = focusNode.next;
            ++size;
            if (size >= size()){
                return false;
            }
    }
        }
        return false;
}

    @Override
    public Iterator iterator() {
        return  null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

//    @Override
//    public boolean add(java.lang.Object o) {
//        return false;
//    }

    @Override
    public boolean add(Object o) {
        if (head == null) {
            Node newNode = new Node(o);
            head = newNode;


        } else if (head.next == null) {
            Node newNode = new Node(o);
            head.next = newNode;
        } else {
            Node  focusNode = head;
            while (focusNode.next != null) {
                focusNode = focusNode.next;
            }
            Node newNode = new Node(o);
            focusNode.next = newNode;
            return true;
        }
        return false;
    }

    @Override
    public boolean remove(Object o) {
        Node   focusNode = head;
//        while (focusNode.next != null) {
//            if (focusNode == o){
//                Node nextNode = focusNode.next;
//                focusNode.next = nextNode.next;
//                return true;
//            }

            for (Iterator iterator = iterator(); iterator().hasNext();){
                if (focusNode == o){
                    Node nextNode = focusNode.next;
                    focusNode.next = nextNode.next;
                    return true;

            }
            focusNode = focusNode.next;

        }
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        return false;
    }

    @Override
    public void clear() {}


    @Override
    public Object get(int index) {
        int icount = 0;
        if (isEmpty()){
            throw new IndexOutOfBoundsException();
        }
        if (index == 0) {
            return head.element;
        } else if (index > 0 && index < size()) {
            Node focusNode = head;
            while (icount < index) {
                focusNode = focusNode.next;
                ++icount;
            }
            return focusNode.element;
        } else if (index < 0 || index > size()) {
            throw new IllegalArgumentException();
        }
        else {
            return false;
        }
    }


    @Override
    public Object set(int index, Object element) {
        int icount = 0;
        if (isEmpty()){
            throw new IndexOutOfBoundsException();
        }
        if (index == 0) {
            Node removeNode = head;
            Node  newNode = new Node(element);
            newNode.next = head;
            head = newNode;
            return removeNode.element;

        } else if (index > 0 && index < size()) {
           Node prevNode = new Node(null);
            prevNode.next = head;
            Node  focusNode = head;
            //++icount;
            while (icount < index) {
                focusNode = focusNode.next;
                prevNode = prevNode.next;
                ++icount;
            }
            Node   removeNode = focusNode;
            Node newNode = new Node(element);
            newNode.next = focusNode.next;
           prevNode.next = newNode;
            return removeNode.element;
    }
        else if (index >= size()){
            throw new IndexOutOfBoundsException() ;
        }

        else{
            return null;
        }
    }

    @Override
    public void add(int index, Object element) {
        int icount = 0;
        if (index == 0) {
            Node  newNode = new Node(element);
            newNode.next = head;
            head = newNode;

        } else {
            Node  focusNode = head;
            ++icount;
            while (icount < index) {
                focusNode = focusNode.next;
                ++icount;
            }
            Node newNode = new Node(element);
            newNode.next = focusNode.next;
            focusNode.next = newNode;

        }

    }

    @Override
    public Object remove(int index) {
//        int icount = 0;
//        if (index == 0) {
//            head = head.next;
//        } else  if (index > 0 && index < size()){
//            Node  focusNode = head;
//            ++icount;
//            while (icount < index) {
//                focusNode = focusNode.next;
//                ++icount;
//            }
//            Node  nextNode = focusNode.next;
//            focusNode.next = nextNode.next;
//            return focusNode;
//        }
//        return null;
        Node focusNode = head;
        for (int i=0 ; i < index -1; i++){
           focusNode = focusNode.next;
        }

        Object nextNode = focusNode.next.element;
        focusNode.next = focusNode.next.next;
        return nextNode;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator listIterator() {
        return null;
    }

    @Override
    public ListIterator listIterator(int index) {
        return null;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }


}