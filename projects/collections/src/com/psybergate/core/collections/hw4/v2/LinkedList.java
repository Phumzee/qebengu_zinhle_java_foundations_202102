package com.psybergate.core.collections.hw4.v2;

import com.psybergate.core.collections.hw4.v3.Node;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class LinkedList implements List {


    private Object head;






    @Override
    public int size() {
        int icount = 0;
        Object focusNode = head;
        while (focusNode.next != null) {
            focusNode = focusNode.next;
            ++icount;
        }
        ++icount;
        return icount;
    }


    @Override
    public boolean isEmpty() {
        if (size() == 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean contains(java.lang.Object o) {
        Object focusNode = head;
        for (int i = 0; i < size(); ++i) {
            if (focusNode == o) {
                return true;
            }
            focusNode = focusNode.next;
        }

        return false;

    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public java.lang.Object[] toArray() {
        return new java.lang.Object[0];
    }

    @Override
    public boolean add(java.lang.Object o) {
        if (head == null) {
            head = (Object) o;

        } else if (head.next == null) {
            head.next = (Object) o;
        } else {
            Object  focusNode = head;
            while (focusNode.next != null) {
                focusNode = focusNode.next;
            }
            focusNode.next = (Object) o;
            return true;
        }
        return false;
    }

    @Override
    public boolean remove(java.lang.Object o) {
        Object  focusNode = head;
        while (focusNode.next != null) {
            if (focusNode == o){
                Object nextNode = focusNode.next;
                focusNode.next = nextNode.next;
                return true;
            }
            focusNode = focusNode.next;
        }
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        return false;
    }

    @Override
    public void clear() {


    }

    @Override
    public java.lang.Object get(int index) {
        int icount = 0;
        if (index == 0) {
            return head;
        } else if (index > 0 && index < size()) {
            Object focusNode = head;
            while (icount < index) {
                focusNode = focusNode.next;
                ++icount;
            }
            return focusNode;
        } else {
            return null;
        }
    }


    @Override
    public java.lang.Object set(int index, java.lang.Object element) {
        return null;
    }

    @Override
    public void add(int index, java.lang.Object element) {
        int icount = 0;
        if (index == 0) {
            Object  focusNode = head;
            head = (Object) element;
            ((Object) element).next = focusNode;
        } else {
            Object focusNode = head;
            ++icount;
            while (icount < index) {
                focusNode = focusNode.next;
                ++icount;
            }
            Object  nextNode = focusNode.next;
            focusNode.next = (Object) element;
            ((Object) element).next = nextNode;

        }

    }

    @Override
    public java.lang.Object remove(int index) {
        int icount = 0;
        if (index == 0) {
            head = head.next;
        } else  if (index > 0 && index < size()){
            Object  focusNode = head;
            ++icount;
            while (icount < index) {
                focusNode = focusNode.next;
                ++icount;
            }
            Object  nextNode = focusNode.next;
            focusNode.next = nextNode.next;
            return focusNode;
        }
        return null;
    }

    @Override
    public int indexOf(java.lang.Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(java.lang.Object o) {
        return 0;
    }

    @Override
    public ListIterator listIterator() {
        return null;
    }

    @Override
    public ListIterator listIterator(int index) {
        return null;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public java.lang.Object[] toArray(java.lang.Object[] a) {
        return new java.lang.Object[0];
    }

    //   @Override
    public boolean contains(Object o) {
        Object focusNode = head;
        for (int i = 0; i < size(); ++i) {
            if (focusNode == o) {
                return true;
            }
            focusNode = focusNode.next;
        }

        return false;
    }

}