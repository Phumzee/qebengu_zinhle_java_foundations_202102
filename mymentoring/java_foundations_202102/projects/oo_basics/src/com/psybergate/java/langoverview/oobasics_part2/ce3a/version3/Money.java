package com.psybergate.java.langoverview.oobasics_part2.ce3a.version3;

public final  class Money {

    private String country;
    private String code;
    private double amount;

    public Money(String country, String code) {
        this.country = country;
        this.code = code;

    }

    @Override
    public String toString() {
        return "Money{" +
                "Country='" + country + '\'' +
                ", code='" + code + '\'' +
                '}';
    }

    public Money changeCode(){
        return new Money(this.country,"USSD");
    }
}
