package com.psybergate.jeefnds.enterprise.hwent3.services;

import com.psybergate.jeefnds.enterprise.hwent3.entities.Audit;
import com.psybergate.jeefnds.enterprise.hwent3.entities.Customer;

import javax.ejb.Local;

@Local
public interface CustomerService {

   public  boolean saveResource(Customer customer, Audit audit);



}
