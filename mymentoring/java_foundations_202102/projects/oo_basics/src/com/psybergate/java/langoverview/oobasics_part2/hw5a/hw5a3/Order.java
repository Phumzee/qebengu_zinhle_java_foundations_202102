package com.psybergate.java.langoverview.oobasics_part2.hw5a.hw5a3;

//import com.psybergate.java.langoverview.oobasics_part2.hw5a.OrderItem;

import java.util.ArrayList;

public abstract class Order {


    private ArrayList<OrderItem> orderItems = new ArrayList<>();
    private static final double MORE_THAN_HALFMILLI = 0.05;
    private static final double MORE_THAN_MILLI = 0.1;
    private static final double TWO_YEAR_DISCOUNT = 0.075;
    private static final double FIVE_YEAR_DISCOUNT = 0.125;
    private  double internationalDisocunt;
    private double discount;



    public ArrayList<OrderItem> getOrderItems() {
        return orderItems;
    }



    public  void addOrderItems(OrderItem od){
        orderItems.add(od);

    }

    public double getDiscount(Customer c){

        if (c.getDiscountType() == "local"){
            discount = getLocalDiscount(c);
        }
        else if (c.getDiscountType() == "international"){
            discount = getInternationalDisocunt();
        }
        return discount;
    }

    public  double getLocalDiscount(Customer c){
        if (c.getCustomerDuration() >=2 && c.getCustomerDuration() <=5){
            discount = TWO_YEAR_DISCOUNT;
        }else if (c.getCustomerDuration() > 5){
            discount  = FIVE_YEAR_DISCOUNT;
        }

        return discount;
    }

    public double getInternationalDisocunt(){
        if (this.getOrderTotal() > 500000 && this.getOrderTotal() < 1000000){
            internationalDisocunt = MORE_THAN_HALFMILLI;
        }else if (this.getOrderTotal()> 1000000){
            internationalDisocunt = MORE_THAN_MILLI;
        }
        return internationalDisocunt;
    }



    public double getOrderTotal(){
        double total =0;
        for (int i=0 ; i< orderItems.size();++i){
          total+=   orderItems.get(i).OrderItemTotal();
        }

        return total;
    }

    public  double applyDiscount(){
       return 1- discount;

    }


}
