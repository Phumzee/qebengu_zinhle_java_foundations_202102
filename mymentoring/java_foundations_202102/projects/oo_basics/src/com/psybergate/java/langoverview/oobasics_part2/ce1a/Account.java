package com.psybergate.java.langoverview.oobasics_part2.ce1a;

public class Account {
    private String accountNum;
    private double balance;

    public Account(String accountNum) {
        this.accountNum = accountNum;
    }

    public Account(String accountNum, double balance) {
        this.accountNum = accountNum;
        this.balance = balance;
        System.out.println("Hey I'm an account.");
    }

    public Account(){

    }



}
