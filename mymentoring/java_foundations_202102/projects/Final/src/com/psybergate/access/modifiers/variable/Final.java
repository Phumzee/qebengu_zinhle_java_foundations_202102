package com.psybergate.access.modifiers.variable;

public class Final {
    final int MAX;

    Final(){
    MAX = 5;}

    public void  changeFinal(int num){
        MAX = num;
    }

    public static void main(String[] args) {
        Final f= new Final();
        System.out.println(f.MAX);
    }
}
