package ce1a.standard;

public interface DateFactory {

    /**
     * Creates  a new <code>Date</code>
     * @throws InvalidDateException if year/month/day are not valid for a date
     */

    public Date createDate(int day, int month, int year) throws InvalidDateException;
}
