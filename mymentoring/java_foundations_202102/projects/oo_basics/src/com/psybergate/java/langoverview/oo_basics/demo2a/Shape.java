package com.psybergate.java.langoverview.oo_basics.demo2a;

import java.util.ArrayList;

public abstract class Shape {
    static ArrayList<Shape>shapes = new ArrayList<>();

    public abstract String getDimensions();
    public abstract double getArea();
    public  void print(){
        System.out.println("This shape has dimensions:" + getDimensions() + "and has an area of" + getArea());
    }

    public static void addShapes(){
        shapes.add(new Circle(5));
        shapes.add(new Rectangle(5, 2));
        shapes.add(new Triangle(4,5));


    }

    public static void printShapes(){
        for (int i=0; i < shapes.size();++i){
            shapes.get(i).print();
        }
    }

    public static void main(String[] args) {
        addShapes();
        printShapes();

    }


}

