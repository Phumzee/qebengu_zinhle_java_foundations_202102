package com.psybergate.grad2021.core.concurrency.demo2;

public class Iterator {

    public int num ;

    public Iterator(int num) {
        this.num = num;
    }

    public void iterate(){
        for (int i=0; i < num ; ++i){
            System.out.println("i = " + i);
        }
    }
}
