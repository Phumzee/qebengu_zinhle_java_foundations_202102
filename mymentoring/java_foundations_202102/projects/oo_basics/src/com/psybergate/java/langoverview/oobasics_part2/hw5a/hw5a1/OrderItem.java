package com.psybergate.java.langoverview.oobasics_part2.hw5a.hw5a1;

//import com.psybergate.java.langoverview.oobasics_part2.hw5a.Product;

public class OrderItem {

    private int quantity;
    private Product product;

    public OrderItem(int quantity, Product product) {
        this.quantity = quantity;
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public double pricePerOrder(){
        return product.getPrice() * quantity;
    }



}
