package com.psybergate.jeefnds.servlets.arch2_v2;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/dispatch/*")
public class DispatcherServlet extends HttpServlet {

    static Map<String, Controller> map = new HashMap<>();
    private static final Controller HelloWorldController = new HelloWorldController();
    private static final Controller GetCurrentDateController = new GetCurrentDateController();

    static {
        map.put("/hello", HelloWorldController);
        map.put("/getcurrentdate",GetCurrentDateController);
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><body>");
        String reqPathInfo = req.getPathInfo();

        Controller controller = getController(reqPathInfo);
        String string = controller.execute(req);
        out.println(string);

        out.println("<html><body>");
        out.close();
    }

    public Controller getController(String reqPathInfo){

        Controller controller  = map.get(reqPathInfo);
        return controller;
//        if (reqPathInfo.equals("/hello")){
//            return HelloWorldController;
//        }
//        else if (reqPathInfo.equals("/date")){
//            return GetCurrentDateController;
//        }
//        throw new RuntimeException("Invalid Request!!");
    }
}
