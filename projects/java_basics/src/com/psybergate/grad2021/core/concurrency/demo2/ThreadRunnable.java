package com.psybergate.grad2021.core.concurrency.demo2;

public class ThreadRunnable implements  Runnable{

    private Iterator iterator;

    public ThreadRunnable(Iterator iterator) {
        this.iterator = iterator;
    }

    @Override
    public void run() {
        iterator.iterate();
    }
}
