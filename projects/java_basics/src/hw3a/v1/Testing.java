package hw3a.v1;

public class Testing {

    public int countifEven(int i) {
        int count = 0;
        if (i % 2 == 0) {
            ++count;
        }
        return count;
    }

    public int countifOdd(int i) {
        int count = 0;
        if (i % 2 != 0) {
            ++count;
        }
        return count;
    }

    public int countIfStartsWithC(String s1) {
        int count = 0;
        if (s1.charAt(0) == 99) {
            ++count;
        }
        return count;
    }

    public int CountIfPrime(int number) {
        int count = 0;
        int sqrt = (int) Math.sqrt(number) + 1;
        for (int i = 2; i < sqrt; i++) {
            if (number % i == 0) {
                ++count;
            }
        }
        return count;
    }

    public int countIfOverdraft(Account account){
        int count = 0;
        if (account.getBalance() > 1000){
            ++count;
        }
        return count;
    }

}
