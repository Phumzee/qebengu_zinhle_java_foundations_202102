package com.psybergate.java.langoverview.oo_basics.demo2a;

public class Rectangle extends Shape {
    private int length;
    private int width;

    public Rectangle(int length, int width){
        super();
        this.length = length;
        this.width = width;
    }



    public double getArea(){
        return length * width;

    }

    public String getDimensions(){
        String s1 = "length" + length + ", width: " +  width;
        return s1;
    }

}
