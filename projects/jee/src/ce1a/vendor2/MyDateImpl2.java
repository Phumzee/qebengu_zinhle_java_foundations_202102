package ce1a.vendor2;

import ce1a.standard.Date;
import ce1a.standard.InvalidDateException;

import static ce1a.standard.InvalidDateException.DAY_INVALID;
import static ce1a.standard.InvalidDateException.YEAR_INVALID;

public class MyDateImpl2 implements Date {

    int year;
    int month;
    int day;


    public MyDateImpl2(int day, int month, int year) throws InvalidDateException {
       if (day <= 0 || day> 31) {
           throw new InvalidDateException(DAY_INVALID, "Day invalid");
       }
       if (month <=0 || month > 12 ){
           throw new InvalidDateException(InvalidDateException.MONTH_INVALID, "Day invalid");
       }
       if (year < 1900 || year > 2500){
           throw new InvalidDateException(YEAR_INVALID, "Day invalid");
       }

       this.year = year;
       this.month = month;
       this.day = day;

    }

    @Override
    public int getYear() {
        return 0;
    }

    @Override
    public int getMonth() {
        return 0;
    }

    @Override
    public int getDay() {
        return 0;
    }

    @Override
    public boolean isLeapYear() {
        return false;
    }

    @Override
    public Date addDays(int numDays) {
        return null;
    }
}
