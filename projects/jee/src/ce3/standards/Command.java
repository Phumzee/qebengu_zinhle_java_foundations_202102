package ce3.standards;

public interface Command extends CommandRequest {


   public void execute();

}
