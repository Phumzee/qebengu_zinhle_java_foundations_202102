package com.psybergate.grad2021.core.annotations.hw2;

 @interface DomainProperty {
     boolean Primarykey() default false;

     boolean isNull() default true;

}
