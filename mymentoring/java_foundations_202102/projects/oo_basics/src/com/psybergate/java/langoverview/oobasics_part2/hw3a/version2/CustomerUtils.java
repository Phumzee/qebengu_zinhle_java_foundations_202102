package com.psybergate.java.langoverview.oobasics_part2.hw3a.version2;

import com.psybergate.java.langoverview.oobasics_part2.hw3a.Customer;

public class CustomerUtils {
    public static void main(String[] args) {
        com.psybergate.java.langoverview.oobasics_part2.hw3a.Customer c1 = new com.psybergate.java.langoverview.oobasics_part2.hw3a.Customer("123", "Person", "She", 12);
        com.psybergate.java.langoverview.oobasics_part2.hw3a.Customer c2 = new com.psybergate.java.langoverview.oobasics_part2.hw3a.Customer("123", "Person", "Her", 14);
        com.psybergate.java.langoverview.oobasics_part2.hw3a.Customer c3 = new Customer("124", "Person", "Hey", 13);

        System.out.println(c1 == c2);
        System.out.println(c1 == c3);
        System.out.println(c1.equals(c2));
        System.out.println(c1.equals(c3));
        System.out.println(c1.equals("abc"));


    }
}
