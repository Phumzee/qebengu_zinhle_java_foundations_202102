package com.psybergate.java.langoverview.oobasics_part2.demo2a;

public class Student {
    private String studentNumber;
    private String name;
    private int yearOfStudy;

    public Student(String studentNumber, String name, int yearOfStudy) {
        this.studentNumber = studentNumber;
        this.name = name;
        this.yearOfStudy = yearOfStudy;
    }

    public boolean equals(Student c){

        if ((c.studentNumber == studentNumber) &&

         (c.name == name) &&

             (c.yearOfStudy == yearOfStudy))
                return true;
            else {
                return false;
            }
    }

}
