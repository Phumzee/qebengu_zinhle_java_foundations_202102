package com.psybergate.java.langoverview.oobasics_part2.hw2a;

public class Rectangle {
    private static final int MAX_LENGTH = 200;
    private static final int MAX_WIDTH = 100;
    private static final int MAX_AREA = 15000;

    private int length;
    private int width;


    public Rectangle(int length, int width){
      /*  isValid();
        if (isValid() == true){
        this.length = length;
        this.width = width;

        }else {
            throw new RuntimeException();
        }*/
        this.length = length;
        this.width = width;
        isValid();
        if (!isValid()){
            throw new RuntimeException("Rectangle invalid, Sorry.");
        }
    }

    public static int getMaxLength(){
        return MAX_LENGTH;
    }

    public static int getMaxWidth(){
        return MAX_WIDTH;
    }

    public static int getMaxArea(){
        return MAX_AREA;
    }


     public int getArea(){
        int area = length * width;
        return area;
     }




    public   boolean  isValid(){
        if ((length > MAX_LENGTH) || (width > MAX_WIDTH) || ((getArea()) > MAX_AREA) || (width > length)){
            return false;
        }
        else {
            return true;
        }}

        public boolean equals(Rectangle r){
        return ((r.length == length) && (r.width == width));

        }

    }



