package com.psybergate.jeefnds.servlets.hwweb1;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

@WebServlet("/servlet")
public class HttpInfoServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><body>");
        out.println("<h1> Protocol:  </h1>");
        out.println("Protocol: " + req.getProtocol());
        out.println("<h1> Path Info:  </h1>");
        out.println("Path Info: " + req.getPathInfo());
        out.println("<h1> Request URI:  </h1>");
        out.println("Request URI: " + req.getRequestURI());
        out.println("<h1> HTTP Method:  </h1>");
        out.println("The HTTP Method: " + req.getMethod());

        Enumeration headerNames = req.getHeaderNames();
        while (headerNames.hasMoreElements()){
            String key = (String) headerNames.nextElement();
            String value = req.getHeader(key);
            out.println(key+" : " + value);
        }

        out.println("</body></html>");
    }




}
