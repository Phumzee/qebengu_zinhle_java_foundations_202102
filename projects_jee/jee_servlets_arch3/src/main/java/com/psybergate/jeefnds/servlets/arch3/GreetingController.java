package com.psybergate.jeefnds.servlets.arch3;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;

public class GreetingController implements Controller{



    public void  helloWorld(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.print("<html><body>");
        out.print("Hello " + req.getParameter("name"));
        out.print("</body></html>");
    }

    public void  goodbyeWorld(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.print("<html><body>");
        out.print("Goodbye " + req.getParameter("name"));
        out.print("</body></html>");
    }


    }

