package com.psybergate.java.langoverview.oobasics_part2.ce3a.version3;

import com.psybergate.java.langoverview.oobasics_part2.ce3a.version2.Money;

public class MoneyUtils {

    public static void main(String[] args) {
        com.psybergate.java.langoverview.oobasics_part2.ce3a.version2.Money m1 = new com.psybergate.java.langoverview.oobasics_part2.ce3a.version2.Money("South Africa", "ZAR");
        com.psybergate.java.langoverview.oobasics_part2.ce3a.version2.Money m2 = new Money("USA", "USSD");
        System.out.println(m1.toString());
        System.out.println(m2.toString());
        m1.changeCode();
        System.out.println(m1.toString());
    }
}
