package com.psybergate.grad2021.core.concurrency.ce3a.v1;

public class PrintDemo {
    private int sumValue;
    public void printCount() {
        try {
            for(int i = 1; i < sumValue; ++i) {
                System.out.println("Counter   ---   "  + i );
                if (i % 500 == 0){
                    break;
                }
            }
        } catch (Exception e) {
            System.out.println("Thread  interrupted.");
        }
    }
}
