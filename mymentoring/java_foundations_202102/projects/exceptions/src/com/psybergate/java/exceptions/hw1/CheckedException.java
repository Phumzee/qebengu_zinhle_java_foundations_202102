package com.psybergate.java.exceptions.hw1;

import com.psybergate.java.exceptions.ce2.MyException;

public class CheckedException {
  public static void main(String[] args) throws MyCheckedException {
    someMethod1();
//    someMethod2();
//    someMethod3();
//    someMethod4();
//    someMethod5();
  }

  public static void someMethod1()  throws MyCheckedException{
    someMethod2();
  }

  public static void someMethod2() throws MyCheckedException{
    someMethod3();
  }

  public static void someMethod3() throws MyCheckedException{
    someMethod4();
  }

  public static void someMethod4() throws MyCheckedException{
    someMethod5();
  }

  public static void someMethod5() throws MyCheckedException {

      throw new MyCheckedException();

    }
  }

