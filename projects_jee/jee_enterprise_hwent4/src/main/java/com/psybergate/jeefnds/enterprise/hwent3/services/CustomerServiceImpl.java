package com.psybergate.jeefnds.enterprise.hwent3.services;

import com.psybergate.jeefnds.enterprise.hwent3.entities.Audit;
import com.psybergate.jeefnds.enterprise.hwent3.entities.Customer;
import com.psybergate.jeefnds.enterprise.hwent3.resources.AuditResource;
import com.psybergate.jeefnds.enterprise.hwent3.resources.CustomerResource;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

@Transactional
@ApplicationScoped
public class CustomerServiceImpl implements  CustomerService{


    @Inject
    CustomerResource customerResource;

    @Inject
    AuditResource auditResource;


    @Override
    public boolean saveResource(Customer customer, Audit audit) {


        customerResource.save(customer);
        auditResource.save(audit);
        return true;
    }


}
