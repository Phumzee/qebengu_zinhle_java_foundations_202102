package com.psybergate.java.exceptions.ce2;

public class ClassA {
  public static void main(String[] args) throws MyException {
    someMethod01();
   someMethod02();
   someMethod03();
     someMethod04();
  }

  public static void someMethod02() {
    throw new MyErrorException();
  }

  public static void someMethod01() throws MyException {
    throw new MyException();
  }

  public static void someMethod03(){
    throw new MyRuntimeException();

  }

  public static void someMethod04() {
    try {
      throw new MyThrowableException();
    } catch (MyThrowableException e) {
      e.printStackTrace();
    }
  }



}
