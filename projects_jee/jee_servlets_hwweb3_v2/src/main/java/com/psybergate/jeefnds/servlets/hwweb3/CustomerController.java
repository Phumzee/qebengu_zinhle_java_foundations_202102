package com.psybergate.jeefnds.servlets.hwweb3;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;

public class CustomerController implements Controller {


        public void addCustomer(HttpServletRequest request, HttpServletResponse response) throws SQLException, ClassNotFoundException, IOException {

            Long customerNum = Long.valueOf(request.getParameter("customernum"));
            String name = request.getParameter("name");
            String surname = request.getParameter("surname");
            LocalDate date = LocalDate.parse(request.getParameter("date"));
            Customer customer = new Customer(customerNum, name, surname, date);
            CustomerService.addCustomer(customer);


            response.sendRedirect("WEB-INF/success.jsp");


        }




    @Override
    public   void execute(HttpServletRequest request, HttpServletResponse response,String method) throws SQLException, IOException, ClassNotFoundException {
       if (method.equals("add")){
           addCustomer(request, response);
       }
    }

}
