package com.psybergate.grad2021.core.concurrency.hw1.v3;

import sun.rmi.runtime.Log;

import java.util.ArrayList;
import java.util.List;

public class MessageCache {

    private boolean write = true;

    public static final MessageCache INSTANCE = new MessageCache();

    private List<String> messages = new ArrayList<>();

    public  synchronized  void add(String message) {
        while(!write){
            try{
                wait();
            } catch (InterruptedException e) {
               Thread.currentThread().interrupt();

            }}
        write = false;
        messages.add(message);
        notify();

    }

    public synchronized String read() {
        while(write){
            try{
                wait();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }}
        write = true;
        notify();
        return messages.get(0);
    }

    public List<String> getMessages() {
        return messages;
    }

    public static MessageCache getINSTANCE() {
        return INSTANCE;
    }


}
