package com.psybergate.grad2021.core.generics.hw3a.v2;

import java.util.Arrays;
import java.util.Collection;

public class Algorithm {
    public static void main(String[] args) {
        Collection<Integer> integers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
        Collection<String> strings = Arrays.asList("crazy", "composure", "babe", "cafe");

        System.out.println("Number of Even integers " + countIf(integers, new EvenPredicate()));
        System.out.println("Number of Odd integers " + countIf(integers, new OddPredicate()));
        System.out.println("Number of Prime integers " + countIf(integers, new PrimePredicate()));



    }


    public static <T> int countIf(Collection<T> c, UnaryPredicate<T> p) {
        int count = 0;
        for (T elem : c)
            if (p.test(elem))
                ++count;
        return count;
    }

}
