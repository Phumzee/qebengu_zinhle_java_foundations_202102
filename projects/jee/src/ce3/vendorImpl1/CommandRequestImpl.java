package ce3.vendorImpl1;

import ce3.standards.Command;
import ce3.standards.CommandRequest;

public class CommandRequestImpl implements CommandRequest {

    Command command;

    public CommandRequestImpl(Command command) {
        this.command = command;
    }

    public Command getCommand() {
        return command;
    }

    @Override
    public void execute() {
          command.execute();
    }
}
