package com.psybergate.java.langoverview.oo_basics.ce4a1;

public class SavingsAccount  extends Account{
     private int cvv;


    public SavingsAccount(String accountNum, double balance, int cvv){
        super(accountNum, balance);
        this.cvv = cvv;
      ;}

      public int getCvv(){
        return cvv;
      }

      public void print(){
        System.out.println("This is a Savings Accounr");
      }

}
