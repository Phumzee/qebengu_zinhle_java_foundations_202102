package com.psybergate.java.langoverview.oobasics_part2.hw2a.version2;
import com.psybergate.java.langoverview.oobasics_part2.hw2a.Rectangle;

public class Main {
    public static void main(String[] args) {


        com.psybergate.java.langoverview.oobasics_part2.hw2a.Rectangle r1 = new com.psybergate.java.langoverview.oobasics_part2.hw2a.Rectangle(12, 6);
        com.psybergate.java.langoverview.oobasics_part2.hw2a.Rectangle r2 = new com.psybergate.java.langoverview.oobasics_part2.hw2a.Rectangle(12, 6);
        com.psybergate.java.langoverview.oobasics_part2.hw2a.Rectangle r3 = new Rectangle(10, 5);

        System.out.println(r1 == r2);
        System.out.println(r1 == r3);
        System.out.println(r1.equals(r2));
        System.out.println(r1.equals(r3));

    }

}
