package com.psybergate.grad2021.core.generics.hw1b;

public class Client {
    public static void main(String[] args) {
        Person p1 = new Person("1234", "James", 36, "captain");
        Person p2 = new Person("2310", "Curry", 34, "Shooter");
        Person p3 = new Person("2300", "Jordyn", 38, "Shooter");
        Person p4 = new Person("1428", "Odom",36, "Shooter" );

        Team t1 = new Team("Lakers");
        t1.addMember(p1);
        t1.addMember(p2);
        t1.addMember(p3);
        t1.addMember(p4);

        System.out.println("t1.getSize() = " + t1.getSize());
        System.out.println("t1.getTeamCaptain() = " + t1.getTeamCaptain());
    }
}
