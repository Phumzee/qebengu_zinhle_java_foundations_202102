package com.psybergate.grad2021.core.generics.hw1b;

import java.util.ArrayList;
import java.util.List;

public class Team {

    private String name;

    public Team(String name) {
        this.name = name;
    }



    List<Person> team = new ArrayList<>();
    private int size;

    public void addMember(Person p){
        team.add(p);
    }

    public String getTeamCaptain(){
        Person teamCaptain = null;
        for (Person p : team) {
            if (p.getPosition() == "captain"){
                teamCaptain = p;
            }
        }

        return teamCaptain.getName();
    }
    public int getSize() {
        return team.size();
    }
}
