package com.psybergate.core.collections.hw1;

import java.util.SortedSet;
import java.util.TreeSet;

//import static com.psybergate.core.collections.hw1.Employee.print;

public class CustomerUtils {
  public static void main(String[] args) {
    SortedSet s1 = new TreeSet(new DecreasingOrder());
    Customer e1 = new Customer(2, "Zee", "manager");
    Customer e2 = new Customer(3, "Noxy", "manager");
    Customer e3 = new Customer(1, "Pearl" ,  "manager");
    Customer e4 = new Customer(3, "Pearl" ,  "manager");
    s1.add(e1);
    s1.add(e1);
    s1.add(e3);
    s1.add(e4);
    print(s1);

    SortedSet s2 = new TreeSet(new IncreasingOrder());

    s2.add(e1);
    s2.add(e1);
    s2.add(e3);
    s2.add(e4);
    print(s2);



  }

  public static  void print(SortedSet s1){
    for (Object e : s1){
      System.out.println(e);
    }
  }
}
