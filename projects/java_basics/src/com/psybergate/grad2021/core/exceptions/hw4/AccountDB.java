package com.psybergate.grad2021.core.exceptions.hw4;

import com.psybergate.grad2021.core.exceptions.hw4.Account;

import java.util.ArrayList;
import java.util.List;

public class AccountDB {

    public static List<Account> accounts = new ArrayList<>();

    public static void addAccount(Account account){
        accounts.add(account);
    }

    public static void deleteAccount(String accountNum){
        for (int i = 0; i < accounts.size(); i++) {
            if (accounts.get(i).getAccountNum() == accountNum){
                accounts.remove(accounts.get(i));
            }
        }
    }

    public void getAccounts(){
        accounts.toString();
    }

    public static Account getAccount(String accountNum){
        Account account = null;
        for (int i = 0; i < accounts.size(); i++) {
            if (accounts.get(i).getAccountNum() == accountNum)
            accounts.get(i).toString();
            account = accounts.get(i);
        }
        return account;
    }

    public static boolean contains(String accountNum){

        for (int i = 0; i < accounts.size(); i++) {
            if (accounts.get(i).getAccountNum() == accountNum){
                return true;
            }
        }
       return false;
    }


}
