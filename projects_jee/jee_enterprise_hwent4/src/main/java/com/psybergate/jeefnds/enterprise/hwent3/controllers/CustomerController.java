package com.psybergate.jeefnds.enterprise.hwent3.controllers;

import com.psybergate.jeefnds.enterprise.hwent3.entities.Audit;
import com.psybergate.jeefnds.enterprise.hwent3.entities.Customer;
import com.psybergate.jeefnds.enterprise.hwent3.services.CustomerService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;

@Named
@ApplicationScoped
public class CustomerController {

  @Inject
  private CustomerService customerService;

  public void addResource(HttpServletRequest request, HttpServletResponse response) {

    Customer customer = new Customer();

    customer.setName("Jane");
    customer.setSurname("Peaches");
    customer.setDateOfBirth(LocalDate.of(1992, 4, 5));

    Audit audit = new Audit();
    audit.setAction("Added Customer.");
    audit.setAuditDate(LocalDate.now());
    audit.setWarnings("None.");

    customerService.saveResource(customer, audit);
  }
}
