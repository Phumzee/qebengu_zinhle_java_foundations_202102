package com.psybergate.core.enums.hw1;

public class YearOfStudy {
  public static final YearOfStudy FIRST_YEAR = new YearOfStudy(4);
  public static final YearOfStudy SECOND_YEAR = new YearOfStudy(3);
  public  static final YearOfStudy THIRD_YEAR = new YearOfStudy(2);



  private int numOfMajors;

  public YearOfStudy(int numOfModules) {
    this.numOfMajors = numOfModules;
  }
}
