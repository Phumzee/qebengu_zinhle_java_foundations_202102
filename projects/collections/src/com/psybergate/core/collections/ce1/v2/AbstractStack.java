package com.psybergate.core.collections.ce1.v2;

import java.util.Collection;
import java.util.Iterator;

public abstract class AbstractStack implements Stack{


    @Override
    public abstract int size();

    @Override
    public abstract boolean isEmpty() ;
    @Override
    public abstract boolean contains(java.lang.Object o);

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public java.lang.Object[] toArray() {
        return new java.lang.Object[0];
    }

    @Override
    public abstract boolean add(java.lang.Object o);

    @Override
    public boolean remove(java.lang.Object o) {
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public java.lang.Object[] toArray(java.lang.Object[] a) {
        return new java.lang.Object[0];
    }



    }

