package com.psybergate.jeefnds.enterprise.hwent0.v2.entities;

import java.time.LocalDate;

public class Audit {

  private String auditNum;

  private LocalDate date;

  public Audit(String auditNum, LocalDate date) {
    this.auditNum = auditNum;
    this.date = date;
  }

  public String getAuditNum() {
    return auditNum;
  }

  public void setAuditNum(String auditNum) {
    this.auditNum = auditNum;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }
}
