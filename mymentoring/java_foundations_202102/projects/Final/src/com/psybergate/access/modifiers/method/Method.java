package com.psybergate.access.modifiers.method;

public class Method {
    private final int SUP;
    private static final  int VAR;

    static {
       VAR = 4;
       throw new RuntimeException();
    }


    public Method(){
        SUP = 5;
    }

    final void sayHi(){
        System.out.println("Hi");

    }

    public static void main(String[] args) {
        Method m = new Method();
        m.sayHi();
    }
}

class OtherClass extends Method{

    public void sayHi(){
        System.out.println("Say Hello");
    }

}

