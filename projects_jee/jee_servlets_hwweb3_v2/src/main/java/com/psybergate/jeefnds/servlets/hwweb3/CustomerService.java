package com.psybergate.jeefnds.servlets.hwweb3;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;

public class CustomerService {

    public  static void addCustomer(Customer customer) throws SQLException, ClassNotFoundException {
        Connection connection = connectDB();
        PreparedStatement st = connection.prepareStatement("insert into customers values(?, ?, ?, ?)");
        st.setLong(1, Long.valueOf(customer.getCustomerNum()));
        st.setString(2, customer.getName());
        st.setString(3, customer.getSurname());
        st.setDate(4, Date.valueOf(customer.getDateOfBirth()));
        st.executeUpdate();

        /*Close all connections*/
        st.close();
        connection.close();
        System.out.println("Data inserted successfully.");

    }

    public  static Connection connectDB() throws ClassNotFoundException, SQLException {

        Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
                    "postgres", "admin");
        } catch (Exception e) {
            throw new RuntimeException("Couldn't connect to database.");
        }
        System.out.println("Opened database successfully");
        return connection;

    }
}
