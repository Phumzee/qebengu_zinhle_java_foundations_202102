package com.psybergate.grad2021.core.annotations.hw3;


@DomainClass
public class Customer {

    @DomainProperty(isNull = false, Primarykey = true)
    private String customerNum;

    @DomainProperty
    private String name;

    @DomainProperty
    private String surname;

    @DomainProperty
    private Integer dateOfBirth;

    @DomainTransient
    private int age;

    public Customer(String customerNum, String name, String surname, Integer dateOfBirth, int age) {
        this.customerNum = customerNum;
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.age = age;
    }

    public String getCustomerNum() {
        return customerNum;
    }


    public String getName() {
        return name;
    }


    public String getSurname() {
        return surname;
    }


    public Integer getDateOfBirth() {
        return dateOfBirth;
    }


    public int getAge() {
        return age;
    }


}
