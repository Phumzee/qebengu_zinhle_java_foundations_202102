package com.psybergate.core.collections.hw1;

public class Customer {
  private int customerNum;
  private String customerName;
  private String customerType;



  public Customer(int customerNum, String customerName, String customerType) {
    this.customerNum = customerNum;
    this.customerName = customerName;
    this.customerType = customerType;


  }

  public int getCustomerNum() {
    return customerNum;
  }

  @Override
  public String toString() {
    return "Customer{" +
            "customerNum=" + customerNum +
            ", customerName='" + customerName + '\'' +
            ", customerType='" + customerType + '\'' +
            '}';
  }


  @Override
  public int hashCode() {
    return 31 + this.customerNum;
  }



  public boolean equals(Object o){
    if (this == o){
      return true;
    }
     if (o == null){
      return false; 
    }
     if (o.getClass() != this.getClass()){
       return false;
     }
    Customer e = (Customer)o;
     return this.customerNum == e.customerNum;
  }


}
