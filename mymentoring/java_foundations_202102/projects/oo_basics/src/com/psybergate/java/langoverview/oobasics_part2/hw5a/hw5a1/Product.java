package com.psybergate.java.langoverview.oobasics_part2.hw5a.hw5a1;

public class Product {
    private String name;
    private double price;

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}
