package com.psybergate.java.langoverview.oo_basics.demo2a;

public class Triangle extends Shape{
    private int base;
    private int height;


    public Triangle(int base, int height){
        this.base = base;
        this.height = height;
    }


    @Override
    public String getDimensions() {
        String s1 = "base: " + base + "height: " + height;
        return s1;
    }

    @Override
    public double getArea() {
        return 0.5 * base * height;
    }
}
