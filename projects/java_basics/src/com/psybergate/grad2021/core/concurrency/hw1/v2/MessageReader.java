package com.psybergate.grad2021.core.concurrency.hw1.v2;

public class MessageReader {

    private  MessageCache messageCache = MessageCache.getINSTANCE();


    public void read(){
        while(!messageCache.isEmpty()){
        System.out.println("messageCache.read() = " + messageCache.read());
        messageCache.getMessages().remove(0);}
    }
}
