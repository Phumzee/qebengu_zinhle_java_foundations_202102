package com.psybergate.java.exceptions.ce2;

public class MyErrorException extends Error {

  public MyErrorException() {
  }

  public MyErrorException(String message) {
    super(message);
  }

  public MyErrorException(String message, Throwable cause) {
    super(message, cause);
  }
}
