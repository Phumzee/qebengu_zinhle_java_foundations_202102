package com.psybergate.grad2021.core.concurrency.hw2;

import java.util.Arrays;

public class VolatileMain {
    private static final int NUM_OF_THREADS = 2;

    public static void main(String[] args) throws InterruptedException {
        VolatileData volatileData = new VolatileData();
        Thread[] threads = new Thread[NUM_OF_THREADS];

        for (int i=0 ; i < NUM_OF_THREADS ; ++i){
            VolatileThread volatileThread = new VolatileThread(volatileData);
            threads[i] = new Thread(volatileThread);

        }

        for  (int i= 0; i < threads.length; ++i){
            threads[i].start();
        }

        for (int i=0 ; i < threads.length ; ++i){
            threads[i].join();
        }
    }
}
