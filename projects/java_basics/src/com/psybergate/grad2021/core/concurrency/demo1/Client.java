package com.psybergate.grad2021.core.concurrency.demo1;

public class Client {
    public static void main(String[] args) {
        MyThread thread = new MyThread();
        MyRunnable myRunnable = new MyRunnable();
        Thread thread1 = new Thread(myRunnable);
        thread.start();
        thread1.start();
    }
}
