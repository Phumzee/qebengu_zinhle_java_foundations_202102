package com.psybergate.java.langoverview.oo_basics.hw5a;

public class Account {
    private String accountNum;
    private double balance;

    public Account(String accountNum, double balance){
        this.accountNum = accountNum;
        this.balance = balance;
    }
    public String getAccountNum(){
        return accountNum;
    }
    public double getBalance(){
        return balance;
    }




}
