package com.psybergate.grad2021.core.reflection.hw1;


import com.psybergate.grad2021.core.reflection.ce2.Student;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;


public class StudentClient {
    public static void main(String[] args) {
        Class clazz = Student.class;
        System.out.println("clazz.getSuperclass() = " + clazz.getSuperclass());
       int modifiers =   clazz.getModifiers();
        //System.out.println(Modifier.toString(method.getM));
        System.out.println("clazz = " + clazz);
        System.out.println("clazz.getPackage() = " + clazz.getPackage());
        getDeclaredAnnotations(clazz);

        System.out.println("clazz.getInterfaces() = " + clazz.getInterfaces());
        getDeclaredFields(clazz);
        getConstructorParameters(clazz);

        getDeclaredMethofs(clazz);

    }

    private static void getDeclaredAnnotations(Class clazz) {
        Annotation[] annotations = clazz.getAnnotations();
        for (Annotation annotation : annotations) {
            System.out.println("annotation = " + annotation.toString());
        }
    }

    private static void getDeclaredFields(Class clazz) {
        Field [] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            System.out.println("field = " + field);
        }
    }

    private static void getConstructorParameters(Class clazz) {
        Constructor[] constructors = clazz.getDeclaredConstructors();
        for (Constructor constructor : constructors) {
            Parameter[] parameters = constructor.getParameters();
            for (Parameter parameter : parameters) {
                System.out.println("parameter = " + parameter);
            }
        }
    }

    private static void getDeclaredMethofs(Class clazz) {
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println("method = " + method);
            System.out.println("Modifier.toString(method.getModifiers()) = " + Modifier.toString(method.getModifiers()));
            System.out.println("method.getReturnType() = " + method.getReturnType());
        }
    }


}
