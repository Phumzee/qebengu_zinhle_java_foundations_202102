package com.psybergate.grad2021.core.annotations.Play;

public class Car {
    private String carname;
    /*CREATE TABLE tblcars (
    id SERIAL PRIMARY KEY,
     carname TEXT NOT NULL,
    brand TEXT NOT NULL
)   ;*/

    private String brand;

    public Car(String carname, String brand) {
        this.carname = carname;
        this.brand = brand;
    }

    public Car() {

    }

    public String getCarname() {
        return carname;
    }

    public void setCarname(String carname) {
        this.carname = carname;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
