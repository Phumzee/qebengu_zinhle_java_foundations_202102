package com.psybergate.core.collections.ce1.v2;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;

public class ArrayStack extends AbstractStack  implements Iterable  {

    private static  Object[] objects = new Object[1000];
    private  int size = 0;

    @Override
    public void push(Object o) {
        objects[this.size] = o;
       ++size;

    }

    @Override
    public Object pop() {
        Object popped;
        if (size < 0){
            popped = null;
        }
        else{
            popped = objects[this.size-1];}



        --size;
    return popped;

    }

    @Override
    public Object get(int position) {

      int   icount = 0;
      while (icount != position){
        ++icount;
      }

     return  objects[icount] ;
    }

    public int size() {
        return this.size;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public boolean contains(java.lang.Object o) {

        for (Iterator iterator = iterator(); iterator().hasNext();){
            Object object = iterator.next();
            if (object.equals(o)){
                return true;
            }
        }
        return false;
    }

    public boolean add(java.lang.Object o) {
        if (o != null){
            return true;
        }
        return false;
    }

    public Iterator iterator() {
        return new MyIterator();
    }


    private  class MyIterator implements  Iterator{
      int position = 0;

       @Override
       public boolean hasNext() {
           return position <= size;
       }

       @Override
       public java.lang.Object next() {
           return objects[position++];
       }
   }



}
