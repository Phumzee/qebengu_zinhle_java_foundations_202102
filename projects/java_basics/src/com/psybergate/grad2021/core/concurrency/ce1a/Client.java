package com.psybergate.grad2021.core.concurrency.ce1a;

import static java.lang.Thread.sleep;

public class Client {
    public static void main(String[] args) throws InterruptedException {
      // doSomething1();
        MyThread myThread = new MyThread();
        sleep(100);
        myThread.start();
        myThread.join();
       doSomething1();
    }

    private static void doSomething1() {
        doSomething2();
    }

    private static void doSomething2() {
        throw new RuntimeException("MAIN IS RUNNING.");
    }
}
